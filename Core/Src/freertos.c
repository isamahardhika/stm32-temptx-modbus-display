/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "semphr.h"
#include "iwdg.h"
#include "spi.h"
#include "usart.h"

#include "config.h"

#include "KeyPad.h"
#include "MAX31865.h"
#include "Modbus.h"
//#include "w25qxx.h"

#include "pt100rtd.h"
#include "flashmem.h"
#include "calculation.h"
#include "conversion.h"
#include "gui.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

float rtdT = 0.0;
float rtdR = 100.0;
float rtdRatio = 0.0;
uint16_t rtdRaw = 0;
uint8_t rtdFault = 0;

uint8_t statusAlarm = 0;

//extern temptx_cfg cfg;

/* USER CODE END Variables */
/* Definitions for modbusTask */
osThreadId_t modbusTaskHandle;
const osThreadAttr_t modbusTask_attributes =
		{ .name = "modbusTask", .stack_size = 2000 * 4, .priority =
				(osPriority_t) osPriorityAboveNormal, };
/* Definitions for thermometerTask */
osThreadId_t thermometerTaskHandle;
const osThreadAttr_t thermometerTask_attributes = { .name = "thermometerTask",
		.stack_size = 2000 * 4, .priority = (osPriority_t) osPriorityNormal, };
/* Definitions for uiTask */
osThreadId_t uiTaskHandle;
const osThreadAttr_t uiTask_attributes = { .name = "uiTask", .stack_size = 2000
		* 4, .priority = (osPriority_t) osPriorityBelowNormal, };

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartTaskModbus(void *argument);
void StartTaskThermometer(void *argument);
void StartTaskUI(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
 * @brief  FreeRTOS initialization
 * @param  None
 * @retval None
 */
void MX_FREERTOS_Init(void) {
	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
	/* USER CODE END RTOS_MUTEX */

	/* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
	/* USER CODE END RTOS_SEMAPHORES */

	/* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
	/* USER CODE END RTOS_TIMERS */

	/* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
	/* USER CODE END RTOS_QUEUES */

	/* Create the thread(s) */
	/* creation of modbusTask */
	modbusTaskHandle = osThreadNew(StartTaskModbus, NULL,
			&modbusTask_attributes);

	/* creation of thermometerTask */
	thermometerTaskHandle = osThreadNew(StartTaskThermometer, NULL,
			&thermometerTask_attributes);

	/* creation of uiTask */
	uiTaskHandle = osThreadNew(StartTaskUI, NULL, &uiTask_attributes);

	/* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
	/* USER CODE END RTOS_THREADS */

	/* USER CODE BEGIN RTOS_EVENTS */
	/* add events, ... */
	vTaskSuspend(thermometerTaskHandle);
	vTaskSuspend(uiTaskHandle);
	/* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartTaskModbus */
/**
 * @brief  Function implementing the modbusTask thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskModbus */
void StartTaskModbus(void *argument) {
	/* USER CODE BEGIN StartTaskModbus */
	printf("StartTaskModbus\r\n");

	bool memIsOk = false;
	memIsOk = mem_init();
	printf("memIsOk? %s\r\n", memIsOk ? "true" : "false");
//	mem_saveDefaultCfg();
	mem_loadCfg((uint8_t*) &cfg);
	vTaskResume(thermometerTaskHandle);
	vTaskResume(uiTaskHandle);
	mem_printCfg();

	huart1.Instance = USART1;
	huart1.Init.BaudRate = (uint32_t) cfg.mbBaudrate * 100;
	switch (cfg.mbParity) {
	case PARITY_NONE:
		huart1.Init.WordLength = UART_WORDLENGTH_8B;
		huart1.Init.StopBits = UART_STOPBITS_2;
		huart1.Init.Parity = UART_PARITY_NONE;
		break;
	case PARITY_EVEN:
		huart1.Init.WordLength = UART_WORDLENGTH_9B;
		huart1.Init.StopBits = UART_STOPBITS_1;
		huart1.Init.Parity = UART_PARITY_EVEN;
		break;
	case PARITY_ODD:
		huart1.Init.WordLength = UART_WORDLENGTH_9B;
		huart1.Init.StopBits = UART_STOPBITS_1;
		huart1.Init.Parity = UART_PARITY_ODD;
		break;
	default:
		huart1.Init.WordLength = UART_WORDLENGTH_9B;
		huart1.Init.StopBits = UART_STOPBITS_1;
		huart1.Init.Parity = UART_PARITY_EVEN;
		break;
	}
	huart1.Init.Mode = UART_MODE_TX_RX;
	huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart1.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huart1) != HAL_OK) {
		Error_Handler();
	}

	modbusHandler_t ModbusH;
	uint16_t ModbusDATA[MAX_MB_ADDR];

	ModbusH.uModbusType = MB_SLAVE;
	ModbusH.port = &huart1;
	ModbusH.u8id = (uint8_t) cfg.mbAddr; //Modbus slave ID
	ModbusH.u16timeOut = 1000;
	ModbusH.EN_Port = NULL;
	ModbusH.u16regs = ModbusDATA;
	ModbusH.u16regsize = sizeof(ModbusDATA) / sizeof(ModbusDATA[0]);
	ModbusH.xTypeHW = USART_HW;
	//Initialize Modbus library
	ModbusInit(&ModbusH);
	//Start capturing traffic on serial Port
	ModbusStart(&ModbusH);
#if 0
	for (uint16_t i = 0; i < MAX_MB_ADDR; i++) {
		ModbusDATA[i] = 0;
	}
#endif
	ModbusDATA[MB_SLAVEID] = cfg.mbAddr;
	ModbusDATA[MB_BAUDRATE] = cfg.mbBaudrate;
	ModbusDATA[MB_PARITY] = cfg.mbParity;
	ModbusDATA[RTOT_EQUATION] = cfg.rtotEquation;
	mb_set_float_abcd(cfg.calZero, ModbusDATA, CAL_ZERO_MSB, CAL_ZERO_LSB);
	mb_set_float_abcd(cfg.calSpan, ModbusDATA, CAL_SPAN_MSB, CAL_SPAN_LSB);
	mb_set_float_abcd(cfg.thresholdLo, ModbusDATA, THRESHOLD_LO_MSB,
			THRESHOLD_LO_LSB);
	mb_set_float_abcd(cfg.thresholdHi, ModbusDATA, THRESHOLD_HI_MSB,
			THRESHOLD_HI_LSB);
	ModbusDATA[SENSOR_TYPE] = SENSOR_PT100;
	ModbusDATA[WIRE_CFG] = WIRE_3;
	ModbusDATA[STATUS_FAULT] = 0;
	ModbusDATA[STATUS_ALARM] = 0;

//	UBaseType_t uxHighWaterMark;

	/* Infinite loop */
	for (;;) {
		HAL_IWDG_Refresh(&hiwdg);

		ModbusDATA[STATUS_FAULT] = (uint16_t) rtdFault;
		ModbusDATA[STATUS_ALARM] = (uint16_t) statusAlarm;
		mb_set_float_abcd(rtdT, ModbusDATA, TEMPERATURE_VAL_MSB,
				TEMPERATURE_VAL_LSB);
		mb_set_float_abcd(rtdR, ModbusDATA, RESISTANCE_VAL_MSB,
				RESISTANCE_VAL_LSB);
//		mb_set_float_abcd(rtdRatio, ModbusDATA, RATIO_VAL_MSB, RATIO_VAL_LSB);

		if (isNewCfg == true) {
//			osDelay(100);
			cfg.mbAddr = ModbusDATA[MB_SLAVEID];
			cfg.mbBaudrate = ModbusDATA[MB_BAUDRATE];
			cfg.mbParity = ModbusDATA[MB_PARITY];
			cfg.rtotEquation = ModbusDATA[RTOT_EQUATION];
			cfg.calZero = mb_get_float_abcd(ModbusDATA, CAL_ZERO_MSB,
					CAL_ZERO_LSB);
			cfg.calSpan = mb_get_float_abcd(ModbusDATA, CAL_SPAN_MSB,
					CAL_SPAN_LSB);
			cfg.thresholdLo = mb_get_float_abcd(ModbusDATA, THRESHOLD_LO_MSB,
					THRESHOLD_LO_LSB);
			cfg.thresholdHi = mb_get_float_abcd(ModbusDATA, THRESHOLD_HI_MSB,
					THRESHOLD_HI_LSB);
			mem_saveCfg((uint8_t*) &cfg);
			mem_printCfg();
			isNewCfg = false;
//			osDelay(100);
			HAL_NVIC_SystemReset();
		}
#if 0
		uxHighWaterMark = uxTaskGetStackHighWaterMark(modbusTaskHandle);
		printf("[TASK MODBUS] STACK SIZE = %lu\r\n", uxHighWaterMark);
#endif
		osDelay(100);
	}
	/* USER CODE END StartTaskModbus */
}

/* USER CODE BEGIN Header_StartTaskThermometer */
/**
 * @brief Function implementing the thermometerTask thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskThermometer */
void StartTaskThermometer(void *argument) {
	/* USER CODE BEGIN StartTaskThermometer */
	printf("StartTaskThermometer\r\n");

	Max31865_t rtd;
	Max31865_init(&rtd, &hspi2, RTD_CS_GPIO_Port, RTD_CS_Pin, WIRE_3,
			MAX31865_FILTER60HZ);
//	osDelay(500);
	Max31865_setThresholds(&rtd, cfg.thresholdRawLo, cfg.thresholdRawHi);
	Max31865_clearFault(&rtd);
//	bool rtdIsOk = false;

//	UBaseType_t uxHighWaterMark;

	/* Infinite loop */
	for (;;) {
		HAL_IWDG_Refresh(&hiwdg);
		HAL_GPIO_TogglePin(LED_BUILTIN_GPIO_Port, LED_BUILTIN_Pin);
//		printf("\r\n");

		if (rtd.lock == 1)
			osDelay(1);
		rtd.lock = 1;

		rtdRaw = Max31865_readRTD(&rtd);

		rtdRatio = pt100rtd_ratio((float) rtdRaw);

//		printf("ADC CODE = %u\r\n", rtdRaw);
//		printf("RATIO = %f\r\n", rtdRatio);

		float ohm, newRes;
		ohm = pt100rtd_resistance(rtdRatio);
		if (ohm >= cfg.calZero) {
//			newRes = mapf(ohm, cfg.calZero, cfg.calSpan, DEFAULT_ZERO,
//			DEFAULT_SPAN);
			newRes = mapf(ohm, cfg.calZero, cfg.calSpan, RES_100OHM,
					RES_150OHM);
		} else {
//			newRes = mapf(ohm, PT100_385_NEG60C, cfg.calZero, PT100_385_NEG60C,
//			DEFAULT_ZERO);
			newRes = mapf(ohm, PT100_385_NEG60C, cfg.calZero, PT100_385_NEG60C,
					RES_100OHM);
		}
#ifdef RESISTANCE_FILTERED
		rtdR = ema_filter(newRes, rtdR, EMA_FACTOR); //smoothing data
#else
		rtdR = newRes;
#endif

		if (cfg.rtotEquation == EQ_RPOLY) {
			rtdT = pt100rtd_celsius_rationalpoly(rtdR);
		} else if (cfg.rtotEquation == EQ_POLY) {
			rtdT = pt100rtd_celsius_polynomial(rtdR);
		} else if (cfg.rtotEquation == EQ_CUBE) {
			rtdT = pt100rtd_celsius_cubic(rtdR);
		} else {
			if (rtdR >= PT100_385_NEG60C)
				rtdT = pt100rtd_celsius_cvd(rtdR);
			else
				rtdT = pt100rtd_celsius_polynomial(rtdR);
		}

		rtdFault = Max31865_readFault(&rtd);

		if (rtdFault == MAX31865_FAULT_NONE) {
			statusAlarm = ALARM_NONE;
			if (rtdT < cfg.thresholdLo) {
				statusAlarm = ALARM_THRESHOLD_LOW;
				printf("[ALARM] THREHSOLD LOW!\r\n");
			} else if (rtdT > cfg.thresholdHi) {
				statusAlarm = ALARM_THRESHOLD_HIGH;
				printf("[ALARM] THREHSOLD HIGH!\r\n");
			}
			printf("[THERMO] Temperature   = %f degC\r\n", rtdT);
			printf("[THERMO] Resistance    = %f ohm\r\n", rtdR);
			printf("[THERMO] Ratio (dR/dT) = %f ohm/degC\r\n", rtdRatio);
			printf("[THERMO] RTD ADC Code  = %u\r\n", rtdRaw);
		} else {
			statusAlarm = ALARM_RTD;
			printf("[THERMO] Fault 0x%02X ", rtdFault);
			if (rtdFault & MAX31865_FAULT_HIGHTHRESH) {
				printf("RTD High Threshold\r\n");
			}
			if (rtdFault & MAX31865_FAULT_LOWTHRESH) {
				printf("RTD Low Threshold\r\n");
			}
			if (rtdFault & MAX31865_FAULT_REFINHIGH) {
				printf("REFIN- > 0.85 x Bias\r\n");
			}
			if (rtdFault & MAX31865_FAULT_REFINLOW) {
				printf("REFIN- < 0.85 x Bias - FORCE- open\r\n");
			}
			if (rtdFault & MAX31865_FAULT_RTDINLOW) {
				printf("RTDIN- < 0.85 x Bias - FORCE- open\r\n");
			}
			if (rtdFault & MAX31865_FAULT_OVUV) {
				printf("Under/Over voltage\r\n");
			}
			Max31865_clearFault(&rtd);
		}
#if 0
		uxHighWaterMark = uxTaskGetStackHighWaterMark(thermometerTaskHandle);
		printf("[TASK THERMOMETER] STACK SIZE = %lu\r\n", uxHighWaterMark);
#endif
//		printf("\r\n");
		osDelay(500);
	}
	/* USER CODE END StartTaskThermometer */
}

/* USER CODE BEGIN Header_StartTaskUI */
/**
 * @brief Function implementing the uiTask thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskUI */
void StartTaskUI(void *argument) {
	/* USER CODE BEGIN StartTaskUI */
	printf("StartTaskUI\r\n");
	KeyPad_Init();
	gui_init();
	osDelay(2000);

//	UBaseType_t uxHighWaterMark;
	/* Infinite loop */
	for (;;) {
		HAL_IWDG_Refresh(&hiwdg);
		gui_loop(rtdT, rtdR, rtdRaw, rtdFault);
//		gui_test();
#if 0
		uxHighWaterMark = uxTaskGetStackHighWaterMark(uiTaskHandle);
		printf("[TASK UI] STACK SIZE = %lu\r\n", uxHighWaterMark);
#endif
		osDelay(1);
	}
	/* USER CODE END StartTaskUI */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

