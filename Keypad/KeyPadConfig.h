#ifndef	_KEYPADCONFIG_H
#define	_KEYPADCONFIG_H
#include "gpio.h"

#define _KEYPAD_DEBOUNCE_TIME_MS 20
#define _KEYPAD_USE_FREERTOS 1
#define _KEYPAD_4X1

#ifndef _KEYPAD_4X1
const GPIO_TypeDef* _KEYPAD_COLUMN_GPIO_PORT[] =
{

};

const uint16_t _KEYPAD_COLUMN_GPIO_PIN[] =
{

};
#endif

const GPIO_TypeDef *_KEYPAD_ROW_GPIO_PORT[] =
{
	KEY1_GPIO_Port,
	KEY2_GPIO_Port,
	KEY3_GPIO_Port,
	KEY4_GPIO_Port
};

const uint16_t _KEYPAD_ROW_GPIO_PIN[] =
{
	KEY1_Pin,
	KEY2_Pin,
	KEY3_Pin,
	KEY4_Pin
};

#endif
