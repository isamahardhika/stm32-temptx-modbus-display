#ifndef	_KEYPAD_H
#define	_KEYPAD_H

#include <stdint.h>
#include <stdbool.h>

/*
 Author:     Nima Askari
 WebSite:    http://www.github.com/NimaLTD
 Instagram:  http://instagram.com/github.NimaLTD
 Youtube:    https://www.youtube.com/channel/UCUhY7qY1klJm1d2kulr9ckw

 Version:    1.0.0


 Reversion History:

 (1.0.0)
 First Release.
 */

//#define KEY_NONE 0x0000
//#define KEY_DEC 0x0101
//#define KEY_INC 0x0201
//#define KEY_DOWN 0x0401
//#define KEY_OK 0x0801

#define KEY_EVENT_RELEASED 0
#define KEY_EVENT_HOLD 1

enum {
	KEY_NONE = 0x0000,
	KEY_BACK = 0x0801,
	KEY_DEC = 0x0401,
	KEY_INC = 0x0201,
	KEY_OK = 0x0101
};

typedef struct {
	uint8_t ColumnSize;
	uint8_t RowSize;
	uint16_t LastKey;

} KeyPad_t;

void KeyPad_Init(void);
uint16_t KeyPad_Scan(void);
uint16_t KeyPad_WaitForKey(uint32_t Timeout_ms);
char KeyPad_WaitForKeyGetChar(uint32_t Timeout_ms);
uint16_t KeyPad_LastKey(void);

#endif
