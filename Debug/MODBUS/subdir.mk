################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (11.3.rel1)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../MODBUS/Modbus.c \
../MODBUS/UARTCallback.c 

OBJS += \
./MODBUS/Modbus.o \
./MODBUS/UARTCallback.o 

C_DEPS += \
./MODBUS/Modbus.d \
./MODBUS/UARTCallback.d 


# Each subdirectory must supply rules for building sources it contributes
MODBUS/%.o MODBUS/%.su MODBUS/%.cyclo: ../MODBUS/%.c MODBUS/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F411xE -c -I../Core/Inc -I"C:/Users/isa/Documents/BINUS ONLINE LEARNING - SKRIPSI THESIS/SDK/stm32-temptx-modbus-display/libs" -I"C:/Users/isa/Documents/BINUS ONLINE LEARNING - SKRIPSI THESIS/SDK/stm32-temptx-modbus-display/Keypad" -I"C:/Users/isa/Documents/BINUS ONLINE LEARNING - SKRIPSI THESIS/SDK/stm32-temptx-modbus-display/MAX31865" -I"C:/Users/isa/Documents/BINUS ONLINE LEARNING - SKRIPSI THESIS/SDK/stm32-temptx-modbus-display/MODBUS" -I"C:/Users/isa/Documents/BINUS ONLINE LEARNING - SKRIPSI THESIS/SDK/stm32-temptx-modbus-display/u8g2_csrc" -I"C:/Users/isa/Documents/BINUS ONLINE LEARNING - SKRIPSI THESIS/SDK/stm32-temptx-modbus-display/W25Q" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-MODBUS

clean-MODBUS:
	-$(RM) ./MODBUS/Modbus.cyclo ./MODBUS/Modbus.d ./MODBUS/Modbus.o ./MODBUS/Modbus.su ./MODBUS/UARTCallback.cyclo ./MODBUS/UARTCallback.d ./MODBUS/UARTCallback.o ./MODBUS/UARTCallback.su

.PHONY: clean-MODBUS

