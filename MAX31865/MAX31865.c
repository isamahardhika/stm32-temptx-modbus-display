/*
 * Refer to:
 * https://github.com/nimaltd/max31865
 * https://github.com/adafruit/Adafruit_MAX31865
 */

#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "max31865.h"
#include "max31865Conf.h"
#if (_MAX31865_USE_FREERTOS == 1)
#include "cmsis_os.h"
#endif

//#########################################################################################################################
#define MAX31865_CONFIG_REG             0x00
#define MAX31865_CONFIG_BIAS            0x80
#define MAX31865_CONFIG_MODEAUTO        0x40
#define MAX31865_CONFIG_MODEOFF         0x00
#define MAX31865_CONFIG_1SHOT           0x20
#define MAX31865_CONFIG_3WIRE           0x10
#define MAX31865_CONFIG_24WIRE          0x00
#define MAX31865_CONFIG_FAULTSTAT       0x02
#define MAX31865_CONFIG_FILT50HZ        0x01
#define MAX31865_CONFIG_FILT60HZ        0x00

#define MAX31865_RTDMSB_REG             0x01
#define MAX31865_RTDLSB_REG             0x02
#define MAX31865_HFAULTMSB_REG          0x03
#define MAX31865_HFAULTLSB_REG          0x04
#define MAX31865_LFAULTMSB_REG          0x05
#define MAX31865_LFAULTLSB_REG          0x06
#define MAX31865_FAULTSTAT_REG          0x07
//#########################################################################################################################
void Max31865_delay(uint32_t delay_ms) {
#if (_MAX31865_USE_FREERTOS == 1)
	osDelay(delay_ms);
#else
  HAL_Delay(delay_ms);
 #endif
}
//#########################################################################################################################
void readRegisterN(Max31865_t *max31865, uint8_t addr, uint8_t *buffer,
		uint8_t n) {
	uint8_t tmp = 0xFF;
	addr &= 0x7F;
	HAL_GPIO_WritePin(max31865->cs_gpio, max31865->cs_pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(max31865->spi, &addr, 1, 100);
	while (n--) {
		HAL_SPI_TransmitReceive(max31865->spi, &tmp, buffer, 1, 100);
		buffer++;
	}
	HAL_GPIO_WritePin(max31865->cs_gpio, max31865->cs_pin, GPIO_PIN_SET);
}
//#########################################################################################################################
uint8_t readRegister8(Max31865_t *max31865, uint8_t addr) {
	uint8_t ret = 0;
	readRegisterN(max31865, addr, &ret, 1);
	return ret;
}
//#########################################################################################################################
uint16_t readRegister16(Max31865_t *max31865, uint8_t addr) {
	uint8_t buffer[2] = { 0, 0 };
	readRegisterN(max31865, addr, buffer, 2);
	uint16_t ret = buffer[0];
	ret <<= 8;
	ret |= buffer[1];
	return ret;
}
//#########################################################################################################################
void writeRegister8(Max31865_t *max31865, uint8_t addr, uint8_t data) {
	HAL_GPIO_WritePin(max31865->cs_gpio, max31865->cs_pin, GPIO_PIN_RESET);
	addr |= 0x80;
	HAL_SPI_Transmit(max31865->spi, &addr, 1, 100);
	HAL_SPI_Transmit(max31865->spi, &data, 1, 100);
	HAL_GPIO_WritePin(max31865->cs_gpio, max31865->cs_pin, GPIO_PIN_SET);
}
//#########################################################################################################################
uint8_t Max31865_readFault(Max31865_t *max31865) {
//	return readRegister8(max31865, MAX31865_FAULTSTAT_REG);
	uint8_t fault = 0;
	fault = readRegister8(max31865, MAX31865_FAULTSTAT_REG);
#ifdef _MAX31865_PRINTFAULT
	if (fault) {
		printf("[MAX31865] Fault 0x%02X ", fault);
		if (fault & MAX31865_FAULT_HIGHTHRESH) {
			printf("RTD High Threshold\r\n");
		}
		if (fault & MAX31865_FAULT_LOWTHRESH) {
			printf("RTD Low Threshold\r\n");
		}
		if (fault & MAX31865_FAULT_REFINHIGH) {
			printf("REFIN- > 0.85 x Bias\r\n");
		}
		if (fault & MAX31865_FAULT_REFINLOW) {
			printf("REFIN- < 0.85 x Bias - FORCE- open\r\n");
		}
		if (fault & MAX31865_FAULT_RTDINLOW) {
			printf("RTDIN- < 0.85 x Bias - FORCE- open\r\n");
		}
		if (fault & MAX31865_FAULT_OVUV) {
			printf("Under/Over voltage\r\n");
		}
	}
#endif
	return fault;
}
//#########################################################################################################################
void Max31865_clearFault(Max31865_t *max31865) {
	uint8_t t = readRegister8(max31865, MAX31865_CONFIG_REG);
	t &= ~0x2C;
	t |= MAX31865_CONFIG_FAULTSTAT;
	writeRegister8(max31865, MAX31865_CONFIG_REG, t);
}
//#########################################################################################################################
void Max31865_enableBias(Max31865_t *max31865, uint8_t enable) {
	uint8_t t = readRegister8(max31865, MAX31865_CONFIG_REG);
	if (enable)
		t |= MAX31865_CONFIG_BIAS;
	else
		t &= ~MAX31865_CONFIG_BIAS;
	writeRegister8(max31865, MAX31865_CONFIG_REG, t);
}
//#########################################################################################################################
void Max31865_autoConvert(Max31865_t *max31865, uint8_t enable) {
	uint8_t t = readRegister8(max31865, MAX31865_CONFIG_REG);
	if (enable)
		t |= MAX31865_CONFIG_MODEAUTO;
	else
		t &= ~MAX31865_CONFIG_MODEAUTO;
	writeRegister8(max31865, MAX31865_CONFIG_REG, t);
}
//#########################################################################################################################
void Max31865_setWires(Max31865_t *max31865, uint8_t numWires) {
	uint8_t t = readRegister8(max31865, MAX31865_CONFIG_REG);
	if (numWires == 3)
		t |= MAX31865_CONFIG_3WIRE;
	else
		t &= ~MAX31865_CONFIG_3WIRE;
	writeRegister8(max31865, MAX31865_CONFIG_REG, t);
}
//#########################################################################################################################
void Max31865_setFilter(Max31865_t *max31865, uint8_t filterHz) {
	uint8_t t = readRegister8(max31865, MAX31865_CONFIG_REG);
	if (filterHz == 50)
		t |= MAX31865_CONFIG_FILT50HZ;
	else
		t &= ~MAX31865_CONFIG_FILT50HZ;
	writeRegister8(max31865, MAX31865_CONFIG_REG, t);
}
/*!
    @brief Read the raw 16-bit value from the RTD_REG in one shot mode
    @return The raw unsigned 16-bit value, NOT temperature!
*/
uint16_t Max31865_readRTD(Max31865_t *max31865) {
	Max31865_clearFault(max31865);
	Max31865_enableBias(max31865, 1);
	Max31865_delay(10);
	uint8_t t = readRegister8(max31865, MAX31865_CONFIG_REG);
	t |= MAX31865_CONFIG_1SHOT;
	writeRegister8(max31865, MAX31865_CONFIG_REG, t);
	Max31865_delay(66);
	uint16_t rtd = readRegister16(max31865, MAX31865_RTDMSB_REG);
	Max31865_enableBias(max31865, 0); // Disable bias current again to reduce selfheating.
	rtd >>= 1; // remove fault
	return rtd;
}
//#########################################################################################################################
void Max31865_init(Max31865_t *max31865, SPI_HandleTypeDef *spi,
		GPIO_TypeDef *cs_gpio, uint16_t cs_pin, uint8_t numwires,
		uint8_t filterHz) {
	if (max31865->lock == 1)
		Max31865_delay(1);
	max31865->lock = 1;
	max31865->spi = spi;
	max31865->cs_gpio = cs_gpio;
	max31865->cs_pin = cs_pin;
	HAL_GPIO_WritePin(max31865->cs_gpio, max31865->cs_pin, GPIO_PIN_SET);
	Max31865_delay(100);
	Max31865_setWires(max31865, numwires);
	Max31865_enableBias(max31865, 0);
	Max31865_autoConvert(max31865, 0);
//	Max31865_setThresholds(max31865, 0, 0xFFFF);
	Max31865_setFilter(max31865, filterHz);
	Max31865_clearFault(max31865);
}
//#########################################################################################################################
bool Max31865_readAll(Max31865_t *max31865, float *readTemp, float *res,
		float *rat, uint16_t *adc, uint8_t *fc) {
	if (max31865->lock == 1)
		Max31865_delay(1);
	max31865->lock = 1;

	bool isOk = false;
	float Z1, Z2, Z3, Z4, Rt, temp;
	uint16_t ADCcode = 0;
	uint8_t Fault = 0;
	*fc = Fault;

	ADCcode = Max31865_readRTD(max31865);
	*adc = ADCcode;
	Rt = (float) ADCcode;
//	printf("[MAX31865] ADC code   = %.0f\r\n", Rt);

	Rt /= (float) 32768;
	*rat = Rt;
//	printf("[MAX31865] Ratio      = %f\r\n", Rt);

	Rt *= _MAX31865_RREF;
	*res = Rt;
//	printf("[MAX31865] Resistance = %f ohm\r\n", Rt);

	Z1 = -iCVD_A;
	Z2 = iCVD_A * iCVD_A - (4 * iCVD_B);
	Z3 = (4 * iCVD_B) / _MAX31865_RNOMINAL;
	Z4 = 2 * iCVD_B;

	temp = Z2 + (Z3 * Rt);
	temp = (sqrtf(temp) + Z1) / Z4;

	if (temp >= 0) {
		*readTemp = temp;
		Fault = Max31865_readFault(max31865);
		*fc = Fault;
		if (Fault == 0)
			isOk = true;
		max31865->lock = 0;
		return isOk;
	}

	Rt /= _MAX31865_RNOMINAL;
	Rt *= 100;

	float rpoly = Rt;

	temp = -242.02;
	temp += 2.2228 * rpoly;
	rpoly *= Rt;  // square
	temp += 2.5859e-3 * rpoly;
	rpoly *= Rt;  // ^3
	temp -= 4.8260e-6 * rpoly;
	rpoly *= Rt;  // ^4
	temp -= 2.8183e-8 * rpoly;
	rpoly *= Rt;  // ^5
	temp += 1.5243e-10 * rpoly;

	*readTemp = temp;

	Fault = Max31865_readFault(max31865);
	*fc = Fault;
	if (Fault == 0)
		isOk = true;
	max31865->lock = 0;

	return isOk;
}
//#########################################################################################################################
#if 0
bool Max31865_readTempF(Max31865_t *max31865, float *readTemp, float *res,
		float *rat, uint16_t *adc) {
	bool isOk = Max31865_readTempC(max31865, readTemp, res, rat, adc);
	*readTemp = (*readTemp * 9.0f / 5.0f) + 32.0f;
	return isOk;
}
#endif
//#########################################################################################################################

/*!
 @brief Write the lower and upper values into the threshold fault
 register to values as returned by readRTD()
 @param lower raw lower threshold
 @param upper raw upper threshold
 */
void Max31865_setThresholds(Max31865_t *max31865, uint16_t lower,
		uint16_t upper) {
	writeRegister8(max31865, MAX31865_LFAULTLSB_REG, lower & 0xFF);
	writeRegister8(max31865, MAX31865_LFAULTMSB_REG, lower >> 8);
	writeRegister8(max31865, MAX31865_HFAULTLSB_REG, upper & 0xFF);
	writeRegister8(max31865, MAX31865_HFAULTMSB_REG, upper >> 8);
}

/*!
 @brief Read the raw 16-bit lower threshold value
 @return The raw unsigned 16-bit value, NOT temperature!
 */
uint16_t Max31865_getLowerThreshold(Max31865_t *max31865) {
	return readRegister16(max31865, MAX31865_LFAULTMSB_REG);
}

/*!
 @brief Read the raw 16-bit lower threshold value
 @return The raw unsigned 16-bit value, NOT temperature!
 */
uint16_t Max31865_getUpperThreshold(Max31865_t *max31865) {
	return readRegister16(max31865, MAX31865_HFAULTMSB_REG);
}
#if 0
/*!
 @brief Calculate the temperature in C from the RTD through calculation of
 the resistance. Uses
 http://www.analog.com/media/en/technical-documentation/application-notes/AN709_0.pdf
 technique
 @param adcCode The raw 16-bit value from the RTD_REG
 @returns Temperature in C
 */
float resistanceToTemperature(uint16_t adcCode) {
	float Z1, Z2, Z3, Z4, Rt, temp;

	Rt = adcCode;
	Rt /= 32768;
	Rt *= _MAX31865_RREF;

	Z1 = -iCVD_A;
	Z2 = iCVD_A * iCVD_A - (4 * iCVD_B);
	Z3 = (4 * iCVD_B) / _MAX31865_RNOMINAL;
	Z4 = 2 * iCVD_B;

	temp = Z2 + (Z3 * Rt);
	temp = (sqrt(temp) + Z1) / Z4;

	if (temp >= 0)
		return temp;

	// ugh.
	Rt /= _MAX31865_RNOMINAL;
	Rt *= 100; // normalize to 100 ohm

	float rpoly = Rt;

	temp = -242.02;
	temp += 2.2228 * rpoly;
	rpoly *= Rt; // square
	temp += 2.5859e-3 * rpoly;
	rpoly *= Rt; // ^3
	temp -= 4.8260e-6 * rpoly;
	rpoly *= Rt; // ^4
	temp -= 2.8183e-8 * rpoly;
	rpoly *= Rt; // ^5
	temp += 1.5243e-10 * rpoly;

	return temp;
}

/*!
 @brief Read the temperature in C from the RTD through calculation of the
 resistance. Uses
 http://www.analog.com/media/en/technical-documentation/application-notes/AN709_0.pdf
 technique
 @returns Temperature in C
 */
float Max31865_readTemperature(Max31865_t *max31865) {
	return resistanceToTemperature(Max31865_readRTD(max31865));
}
#endif
