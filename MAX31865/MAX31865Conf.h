#ifndef _MAX31865CONF_H
#define _MAX31865CONF_H

#define _MAX31865_USE_FREERTOS  1  
//#define _MAX31865_PRINTFAULT

#define _MAX31865_RREF      430.0f	// 430R or 4300R
#define _MAX31865_RNOMINAL  100.0f	// PT100 or PT1000

#define iCVD_A 3.9083e-3
#define iCVD_B -5.775e-7

#endif
