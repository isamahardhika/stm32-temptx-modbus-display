/*
 * Refer to:
 * https://github.com/nimaltd/max31865
 * https://github.com/adafruit/Adafruit_MAX31865
 */

#ifndef _MAX31865_H
#define _MAX31865_H

#ifdef __cplusplus
 extern "C" {
#endif

#include "gpio.h"
#include "spi.h"
#include "stdio.h"
#include <stdbool.h>

//#########################################################################################################################

//#define MAX31865_CELSIUS_MIN -200
//#define MAX31865_CELSIUS_MAX 850

#define MAX31865_FILTER50HZ 50
#define MAX31865_FILTER60HZ 60

#define MAX31865_FAULT_HIGHTHRESH       0x80
#define MAX31865_FAULT_LOWTHRESH        0x40
#define MAX31865_FAULT_REFINHIGH        0x20
#define MAX31865_FAULT_REFINLOW         0x10
#define MAX31865_FAULT_RTDINLOW         0x08
#define MAX31865_FAULT_OVUV             0x04
#define MAX31865_FAULT_NONE				0x00

//#########################################################################################################################

typedef struct {
	GPIO_TypeDef *cs_gpio;
	uint16_t cs_pin;
	SPI_HandleTypeDef *spi;
	uint8_t lock;
} Max31865_t;

//#########################################################################################################################

void Max31865_init(Max31865_t *max31865, SPI_HandleTypeDef *spi,
		GPIO_TypeDef *cs_gpio, uint16_t cs_pin, uint8_t numwires,
		uint8_t filterHz);
bool Max31865_readAll(Max31865_t *max31865, float *readTemp, float *res,
		float *rat, uint16_t *adc, uint8_t *fc);
#if 0
bool Max31865_readTempF(Max31865_t *max31865, float *readTemp, float *res,
		float *rat, uint16_t *adc);
#endif
uint8_t Max31865_readFault(Max31865_t *max31865);
void Max31865_clearFault(Max31865_t *max31865);

//#########################################################################################################################

uint16_t Max31865_readRTD(Max31865_t *max31865);
void Max31865_setThresholds(Max31865_t *max31865, uint16_t lower,
		uint16_t upper);
uint16_t Max31865_getLowerThreshold(Max31865_t *max31865);
uint16_t Max31865_getUpperThreshold(Max31865_t *max31865);
#if 0
float resistanceToTemperature(uint16_t adcCode);
float Max31865_readTemperature(Max31865_t *max31865);
#endif
#ifdef __cplusplus
}
#endif

#endif
