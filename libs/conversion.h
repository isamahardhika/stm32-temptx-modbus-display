/*
 * https://github.com/arduino-libraries/ArduinoModbus/blob/master/src/libmodbus/modbus-data.c
 * https://github.com/DFRobot/STM32/blob/master/libraries/DFRobotBlunoM3/util.h
 * https://github.com/belyalov/stm32-hal-libraries/blob/master/htons.h
 * https://github.com/joeferner/stm32-network/blob/master/interface.h
 */

/*
 * conversion.h
 *
 *  Created on: Nov 21, 2023
 *      Author: isa
 *
 * abcd = little-endian
 * dcba = big-endian
 * badc = little-endian byte swap
 * cdab = big-endian byte swap
 */

#ifndef CONVERSION_H_
#define CONVERSION_H_

/* Sets many bits from a single byte value (all 8 bits of the byte value are
 set) */
void mb_set_bits_from_byte(uint8_t *dest, int idx, const uint8_t value);
/* Sets many bits from a table of bytes (only the bits between idx and
 idx + nb_bits are set) */
void mb_set_bits_from_bytes(uint8_t *dest, int idx, unsigned int nb_bits,
		const uint8_t *tab_byte);
/* Gets the byte value from many bits.
 To obtain a full byte, set nb_bits to 8. */
uint8_t mb_get_byte_from_bits(const uint8_t *src, int idx,
		unsigned int nb_bits);
/* Get a float from 4 bytes (Modbus) without any conversion (ABCD) */
float mb_get_float_abcd(const uint16_t *src, uint8_t idxHi, uint8_t idxLo);
/* Get a float from 4 bytes (Modbus) in inversed format (DCBA) */
float mb_get_float_dcba(const uint16_t *src, uint8_t idxHi, uint8_t idxLo);
/* Get a float from 4 bytes (Modbus) with swapped bytes (BADC) */
float mb_get_float_badc(const uint16_t *src, uint8_t idxHi, uint8_t idxLo);
/* Get a float from 4 bytes (Modbus) with swapped words (CDAB) */
float mb_get_float_cdab(const uint16_t *src, uint8_t idxHi, uint8_t idxLo);
/* DEPRECATED - Get a float from 4 bytes in sort of Modbus format */
float mb_get_float(const uint16_t *src, uint8_t idxHi, uint8_t idxLo);
/* Set a float to 4 bytes for Modbus w/o any conversion (ABCD) */
void mb_set_float_abcd(float f, uint16_t *dest, uint8_t hiIdx, uint8_t loIdx);
/* Set a float to 4 bytes for Modbus with byte and word swap conversion (DCBA) */
void mb_set_float_dcba(float f, uint16_t *dest, uint8_t hiIdx, uint8_t loIdx);
/* Set a float to 4 bytes for Modbus with byte swap conversion (BADC) */
void mb_set_float_badc(float f, uint16_t *dest, uint8_t hiIdx, uint8_t loIdx);
/* Set a float to 4 bytes for Modbus with word swap conversion (CDAB) */
void mb_set_float_cdab(float f, uint16_t *dest, uint8_t hiIdx, uint8_t loIdx);
/* DEPRECATED - Set a float to 4 bytes in a sort of Modbus format! */
void mb_set_float(float f, uint16_t *dest, uint8_t hiIdx, uint8_t loIdx);

#endif /* CONVERSION_H_ */
