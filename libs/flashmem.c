/*
 * flashmem.c
 *
 *  Created on: Nov 28, 2023
 *      Author: MMI
 */

#include "cmsis_os.h"
#include "main.h"
#include "spi.h"
#include "config.h"
#include "w25qxx.h"
#include "flashmem.h"

#define CFG_PAGE_ADDR 0
#define CFG_SECTOR_ADDR 0

temptx_cfg cfg;
w25qxx_t w25q64jv_desc;
w25qxx_peripheral w25q64jv;

void mem_printCfg(void) {
	printf("[MEM] CONFIGURATION:\r\n");
	printf("deviceUid      = 0x%04X\r\n", cfg.deviceUid);
	printf("mbAddr         = %u\r\n", cfg.mbAddr);
	printf("mbBaudrate     = %u\r\n", cfg.mbBaudrate);
	printf("mbParity       = %u\r\n", cfg.mbParity);
	printf("lrv            = %f\r\n", cfg.lrv);
	printf("urv            = %f\r\n", cfg.urv);
	printf("thresholdLo    = %f degC\r\n", cfg.thresholdLo);
	printf("thresholdHi    = %f degC\r\n", cfg.thresholdHi);
	printf("thresholdRawLo = %u\r\n", cfg.thresholdRawLo);
	printf("thresholdRawHi = %u\r\n", cfg.thresholdRawHi);
	printf("calZero        = %f\r\n", cfg.calZero);
	printf("calSpan        = %f\r\n", cfg.calSpan);
	printf("sensorType     = %u\r\n", cfg.sensorType);
	printf("wireCfg        = %u\r\n", cfg.wireCfg);
	printf("toleranceClass = %u\r\n", cfg.toleranceClass);
	printf("tempUnit       = %u\r\n", cfg.tempUnit);
}

bool mem_init(void) {
	printf("[MEM] INIT\r\n");
	w25q64jv.hspi = &hspi1;
	w25q64jv.cs_gpio = MEM_CS_GPIO_Port;
	w25q64jv.cs_pin = MEM_CS_Pin;
	w25q64jv.desc = w25q64jv_desc;
	return W25qxx_Init(&w25q64jv);
}

void mem_loadCfg(uint8_t *cfg_buff) {
	printf("[MEM] LOAD CONFIGURATION\r\n");
	temptx_cfg Config;
	uint32_t cfg_size = sizeof(Config);
	uint32_t cfg_addr = CFG_PAGE_ADDR;
	W25qxx_ReadPage(&w25q64jv, cfg_buff, cfg_addr, 0, cfg_size);
}

void mem_saveCfg(uint8_t *cfg_buff) {
	printf("[MEM] SAVE NEW CONFIGURATION\r\n");
	temptx_cfg Config;
	uint32_t cfg_size = sizeof(Config);
	uint32_t cfg_addr = CFG_PAGE_ADDR;
	uint8_t backup_data[w25q64jv.desc.SectorSize];
	mem_loadCfg(&backup_data[CFG_PAGE_ADDR]);
	memcpy(&backup_data[CFG_PAGE_ADDR], cfg_buff, cfg_size);
	W25qxx_EraseSector(&w25q64jv, CFG_SECTOR_ADDR);
	W25qxx_WritePage(&w25q64jv, &backup_data[cfg_addr], cfg_addr, 0, cfg_size);
}

void mem_saveDefaultCfg(void) {
	printf("[MEM] SAVE DEFAULT CONFIGURATION\r\n");
	cfg.deviceUid = DEFAULT_DEVICE_UID;
	cfg.mbAddr = DEFAULT_MB_SLAVEID;
	cfg.mbBaudrate = BR_19200;
	cfg.mbParity = PARITY_EVEN;
	cfg.lrv = (float) DEFAULT_LRV_C;
	cfg.urv = (float) DEFAULT_URV_C;
	cfg.thresholdLo = (float) DEFAULT_THRESHOLD_LO_C;
	cfg.thresholdHi = (float) DEFAULT_THRESHOLD_HI_C;
	cfg.thresholdRawLo = DEFAULT_THRESHOLD_LO_RAW;
	cfg.thresholdRawHi = DEFAULT_THRESHOLD_HI_RAW;
	cfg.calZero = (float) DEFAULT_ZERO;
	cfg.calSpan = (float) DEFAULT_SPAN;
	cfg.sensorType = SENSOR_PT100;
	cfg.wireCfg = WIRE_3;
	cfg.toleranceClass = CLASS_B;
	cfg.tempUnit = UNIT_CELSIUS;
	cfg.rtotEquation = EQ_CVD;
	mem_saveCfg((uint8_t*) &cfg);
}
