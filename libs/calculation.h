/*
 * calculation.h
 *
 *  Created on: Dec 6, 2023
 *      Author: isa
 */

#ifndef CALCULATION_H_
#define CALCULATION_H_

float ema_filter(float newIn, float lastOut, float factor);
long map(long x, long in_min, long in_max, long out_min, long out_max);
float mapf(float x, float in_min, float in_max, float out_min, float out_max);

#endif /* CALCULATION_H_ */
