/*
 * gui.h
 *
 *  Created on: Nov 22, 2023
 *      Author: isa
 */

#include "stdint.h"

#ifndef GUI_H_
#define GUI_H_

#define _GUI_USE_FREERTOS	1
#define _GUI_DEBUG			1

#define SPACE_ROW 11
#define OFFSET_LEFT 2
#define MAX_MENU_PER_SCREEN 5

enum Menu {
	MENU_HOME = 0,
	MENU_MAIN,
	MENU_MB_SLAVEADDR,
	MENU_MB_BAUDRATE,
	MENU_MB_PARITY,
//	MENU_CALIBRATION,
	MENU_CAL_ZERO,
	MENU_CAL_SPAN,
//	MENU_THRESHOLD,
	MENU_THRES_AUTO_LO,
	MENU_THRES_AUTO_HI,
	MENU_THRES_MANUAL_LO,
	MENU_THRES_MANUAL_HI,
	MENU_RTOT_EQUATION,
	MENU_DEVINFO
};

void gui_init(void);
void gui_test(void);
void gui_loop(float PV, float SV, uint16_t TV, uint8_t QV);

#endif /* GUI_H_ */
