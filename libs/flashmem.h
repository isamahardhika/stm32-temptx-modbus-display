/*
 * flashmem.h
 *
 *  Created on: Nov 28, 2023
 *      Author: MMI
 */

#ifndef FLASHMEM_H_
#define FLASHMEM_H_

typedef union FloatAs32 {
	volatile float f;
	volatile uint32_t u32; // "Overlays" other fields in union
} FloatAs32;

extern temptx_cfg cfg;

void mem_printCfg(void);
bool mem_init(void);
void mem_loadCfg(uint8_t *cfg_buff);
void mem_saveCfg(uint8_t *cfg_buff);
void mem_saveDefaultCfg(void);

#endif /* FLASHMEM_H_ */
