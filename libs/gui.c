/*
 * gui.c
 *
 *  Created on: Nov 22, 2023
 *      Author: isa
 */

#include "gui.h"

#include "main.h"
#if (_GUI_USE_FREERTOS == 1)
#include "cmsis_os.h"
#define GUI_Delay(delay) osDelay(delay)
#else
#define GUI_Delay(delay) HAL_Delay(delay)
#endif
#include "i2c.h"

#include "KeyPad.h"
#include "MAX31865.h"
#include "pt100rtd.h"
#include "flashmem.h"
#include "u8g2.h"
#include "u8x8.h"

#define DISP_I2C_ADDR (0x3C << 1)
#define DISP_I2C_HANDLER hi2c1

u8g2_t u8g2;
char dispBuf[128];
uint8_t menu = MENU_HOME;
uint16_t key = KEY_NONE;
uint8_t cursor = 0;

float thres_lo_c_temp = 0;
float thres_lo_raw_temp = 0;
float thres_hi_c_temp = 0;
float thres_hi_raw_temp = 0;
float cal_zero_temp = 0;
float cal_span_temp = 0;
uint8_t mb_slave_addr_temp = 0;
uint8_t pagenum = 0, numofpages = 0;

//extern temptx_cfg cfg;

uint8_t u8x8_gpio_and_delay_stm32hal(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int,
		void *arg_ptr) {
	switch (msg) {
	case U8X8_MSG_GPIO_AND_DELAY_INIT: // called once during init phase of u8g2/u8x8
		GUI_Delay(1);
		break;							// can be used to setup pins
	case U8X8_MSG_DELAY_NANO:			// delay arg_int * 1 nano second
//		for (uint32_t i = 1; i <= arg_int * 10; i++)
//		__NOP();
		break;
	case U8X8_MSG_DELAY_100NANO:		// delay arg_int * 100 nano seconds
//		__NOP();
		break;
	case U8X8_MSG_DELAY_10MICRO:		// delay arg_int * 10 micro seconds
//		for (uint16_t n = 0; n < 320; n++)
//		__NOP();
		break;
	case U8X8_MSG_DELAY_MILLI:			// delay arg_int * 1 milli second
		GUI_Delay(arg_int);
		break;
	case U8X8_MSG_DELAY_I2C:// arg_int is the I2C speed in 100KHz, e.g. 4 = 400 KHz
		break;			// arg_int=1: delay by 5us, arg_int = 4: delay by 1.25us
	case U8X8_MSG_GPIO_I2C_CLOCK:	// arg_int=0: Output low at I2C clock pin
		break;		// arg_int=1: Input dir with pullup high for I2C clock pin
	case U8X8_MSG_GPIO_I2C_DATA:		// arg_int=0: Output low at I2C data pin
		break;		// arg_int=1: Input dir with pullup high for I2C data pin
	default:
		u8x8_SetGPIOResult(u8x8, 1);			// default return value
		break;
	}
	return 1;
}

uint8_t u8x8_byte_i2c_stm32hal(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int,
		void *arg_ptr) {
	static uint8_t buffer[32]; /* u8g2/u8x8 will never send more than 32 bytes between START_TRANSFER and END_TRANSFER */
	static uint8_t buf_idx;
	uint8_t *data;

	switch (msg) {
	case U8X8_MSG_BYTE_SEND:
		data = (uint8_t*) arg_ptr;
		while (arg_int > 0) {
			buffer[buf_idx++] = *data;
			data++;
			arg_int--;
		}
		break;
	case U8X8_MSG_BYTE_INIT:
		/* add your custom code to init i2c subsystem */
		break;
	case U8X8_MSG_BYTE_SET_DC:
		/* ignored for i2c */
		break;
	case U8X8_MSG_BYTE_START_TRANSFER:
		buf_idx = 0;
		break;
	case U8X8_MSG_BYTE_END_TRANSFER:
//		i2c_transfer(u8x8_GetI2CAddress(u8x8) >> 1, buf_idx, buffer);
//		uint8_t iaddress = I2C_ADDRESS;
//		HAL_I2C_Master_Transmit_DMA(&hi2c1, (uint16_t)iaddress<<1, &buffer[0], buf_idx);
		HAL_I2C_Master_Transmit(&DISP_I2C_HANDLER, DISP_I2C_ADDR,
				(uint8_t*) buffer, buf_idx, 1000);
		//TODO Investigate why delay is needed here.
		//Seems like DMA feeding bytes too fast.
//		for (uint32_t i = 1; i <= 500; i++)
//		__NOP();
		break;
	default:
		return 0;
	}
	return 1;
}

void gui_init(void) {
//	u8g2_Setup_sh1106_i2c_128x64_noname_f(&u8g2, U8G2_R0, u8x8_byte_sw_i2c, u8x8_gpio_and_delay_stm32);
	u8g2_Setup_sh1106_i2c_128x64_noname_f(&u8g2, U8G2_R0,
			u8x8_byte_i2c_stm32hal, u8x8_gpio_and_delay_stm32hal);
	u8g2_InitDisplay(&u8g2); // send init sequence to the display, display is in sleep mode after this,
	u8g2_SetPowerSave(&u8g2, 0); // wake up display
	u8g2_ClearDisplay(&u8g2);
	u8g2_SetDrawColor(&u8g2, 1);
#if 1
	u8g2_SetFont(&u8g2, u8g2_font_profont11_mf);
//	u8g2_SetFont(&u8g2, u8g2_font_spleen5x8_mf);
//	u8g2_SetFontDirection(&u8g2, 0);
	sprintf(dispBuf, "MODBUS RTU (RS-485)");
	uint16_t strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	uint16_t xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 8, dispBuf);

	sprintf(dispBuf, "RTD PT100 3-WIRE");
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 19, dispBuf);

	sprintf(dispBuf, "THERMOMETERS");
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 30, dispBuf);

	sprintf(dispBuf, "M.M.ISA (2402013711)");
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 41, dispBuf);

	sprintf(dispBuf, "MUHAMMAD.ISA");
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 52, dispBuf);

	sprintf(dispBuf, "@BINUS.AC.ID");
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 63, dispBuf);

	u8g2_SendBuffer(&u8g2);
#endif
	menu = MENU_HOME;
}

void gui_test(void) {
	float randnum = (float) rand() / 10000000;
	if ((int) randnum % 2 == 0)
		randnum *= -1;
	u8g2_ClearBuffer(&u8g2);
	u8g2_SetFont(&u8g2, u8g2_font_7_Seg_33x19_mn);
	sprintf(dispBuf, "%.1f", randnum);
	u8g2_DrawStr(&u8g2, -2, -2, dispBuf);
	printf("DISPLAYS: %s\r\n", dispBuf);
	sprintf(dispBuf, "%cC", 176);
//	u8g2_SetFont(&u8g2, u8g2_font_9x18B_mf);
//	u8g2_SetFont(&u8g2, u8g2_font_spleen12x24_mf);
	u8g2_SetFont(&u8g2, u8g2_font_profont22_mf);
	if (randnum >= 0.0 && randnum < 10.0) {
		u8g2_DrawStr(&u8g2, 106 - 38, 16, dispBuf);
	} else if ((randnum >= 10.0 && randnum < 100.0)
			|| (randnum < 0.0 && randnum > -10.0)) {
		u8g2_DrawStr(&u8g2, 106 - 19, 16, dispBuf);
	} else if ((randnum >= 100) || (randnum <= -10.0 && randnum > -100.0)) {
		u8g2_DrawStr(&u8g2, 106, 16, dispBuf);
	} else if (randnum <= -100.0) {
		u8g2_SetFont(&u8g2, u8g2_font_profont17_mf);
		u8g2_DrawStr(&u8g2, 111, 12, dispBuf);
	}
	u8g2_SendBuffer(&u8g2);
}

//PV = temperature, QV = fault
void menu_home(float PV, uint8_t QV) {
	u8g2_ClearBuffer(&u8g2);

	if (PV >= 0.0 && PV < 10.0) {
		sprintf(dispBuf, "%.4f", PV);
	} else if ((PV >= 10.0 && PV < 100.0) || (PV < 0.0 && PV > -10.0)) {
		sprintf(dispBuf, "%.3f", PV);
	} else if (PV >= 100.0 || PV <= -10.0) {
		sprintf(dispBuf, "%.2f", PV);
	}
	if (PV < DEFAULT_LRV_C || PV > DEFAULT_URV_C) {
		sprintf(dispBuf, "OUT OF RANGE");
		u8g2_SetFont(&u8g2, u8g2_font_profont17_mf);
		u8g2_DrawStr(&u8g2, 0, 11, dispBuf);
	} else {
		u8g2_SetFont(&u8g2, u8g2_font_7_Seg_41x21_mn);
		u8g2_DrawStr(&u8g2, 1, -2, dispBuf);
	}
//	printf("[GUI] Displayed PV: %s degC\r\n", dispBuf);
#if 0
		if (PV <= -100.0) {
			u8g2_SetFont(&u8g2, u8g2_font_profont17_mf);
			u8g2_DrawStr(&u8g2, 111, 64, dispBuf);
		} else {
			u8g2_SetFont(&u8g2, u8g2_font_profont22_mf);
			u8g2_DrawStr(&u8g2, 106, 64, dispBuf);
		}
#endif

	if (QV == MAX31865_FAULT_NONE) {
		u8g2_SetFont(&u8g2, u8g2_font_spleen16x32_mf);
//		u8g2_SetFont(&u8g2, u8g2_font_profont29_mf);
		sprintf(dispBuf, "%cC", 176);
		u8g2_DrawStr(&u8g2, 98, 64, dispBuf);
	} else {
		u8g2_SetFont(&u8g2, u8g2_font_spleen8x16_mf);
//		u8g2_SetFont(&u8g2, u8g2_font_profont29_mf);
		sprintf(dispBuf, "%cC", 176);
		u8g2_DrawStr(&u8g2, 112, 51, dispBuf);

		u8g2_SetFont(&u8g2, u8g2_font_profont11_mf);
		sprintf(dispBuf, "RTD FAULT!");
		u8g2_DrawStr(&u8g2, 0, 51, dispBuf);
		if (QV & MAX31865_FAULT_HIGHTHRESH) {
			sprintf(dispBuf, "RTD HIGH THRESHOLD");
		}
		if (QV & MAX31865_FAULT_LOWTHRESH) {
			sprintf(dispBuf, "RTD LOW THREHSOLD");
		}
		if (QV & MAX31865_FAULT_REFINHIGH) {
			sprintf(dispBuf, "VREFIN HIGH");
		}
		if (QV & MAX31865_FAULT_REFINLOW) {
			sprintf(dispBuf, "VREFIN LOW|FORCE-open");
		}
		if (QV & MAX31865_FAULT_RTDINLOW) {
			sprintf(dispBuf, "VRTDIN LOW|FORCE-open");
		}
		if (QV & MAX31865_FAULT_OVUV) {
			sprintf(dispBuf, "Under/Over Voltage");
		}
		u8g2_DrawStr(&u8g2, 0, 62, dispBuf);
	}

	u8g2_SendBuffer(&u8g2);

	if (key == KEY_OK) {
		menu = MENU_MAIN;
		cursor = 0;
	}
}

#define NUM_EQ 5
const char *equationstr[] = { "CALLENDAR-VAN DUSEN", "CUBIC", "POLYNOMIAL",
		"RATIONAL POLYNOMIAL", "EXIT/BACK", NULL };
void menu_rtot_equation(void) {
	u8g2_ClearBuffer(&u8g2);

	uint8_t ycoord = 0, startmenu = 0;
	u8g2_SetFont(&u8g2, u8g2_font_profont11_tr);
	u8g2_SetDrawColor(&u8g2, 2);
	u8g2_SetFontMode(&u8g2, 1);
	ycoord += 7;
	sprintf(dispBuf, "SET T(R) EQUATION");
	uint16_t strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	uint16_t xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, ycoord, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, ycoord + 1, 128);
	ycoord += 2;
	if (key == KEY_DEC) {
		if (cursor < NUM_EQ - 1) {
			cursor++;
		} else {
			cursor = 0;
		}
	} else if (key == KEY_INC) {
		if (cursor > 0) {
			cursor--;
		} else {
			cursor = NUM_EQ - 1;
		}
	} else if (key == KEY_BACK) {
		menu = MENU_MAIN;
		cursor = NUM_EQ - 1;
	}
	if (cursor > MAX_MENU_PER_SCREEN - 2) {
		startmenu = cursor - (MAX_MENU_PER_SCREEN - 2);
	}
	//printf("awal:%d, cn:%d\r\n",startmenu,cursor);
	for (uint8_t i = startmenu; i < startmenu + MAX_MENU_PER_SCREEN; i++) {
		ycoord += SPACE_ROW;
		if (equationstr[i] == NULL)
			break;
		u8g2_DrawStr(&u8g2, OFFSET_LEFT, ycoord, equationstr[i]);
		if (cursor == i) {
			u8g2_DrawBox(&u8g2, 0, ycoord - SPACE_ROW + 2, 128, SPACE_ROW);
		}
		//printf("i:%d, ycoord:%d,%s\r\n",i,ycoord,equationstr[i]);
	}

	u8g2_SendBuffer(&u8g2);

	if (key == KEY_OK) {
		switch (cursor) {
		case 0:
			cfg.rtotEquation = EQ_CVD;
			break;
		case 1:
			cfg.rtotEquation = EQ_CUBE;
			break;
		case 2:
			cfg.rtotEquation = EQ_POLY;
			break;
		case 3:
			cfg.rtotEquation = EQ_RPOLY;
			break;
		case 4:
			menu = MENU_MAIN;
			break;
		default:
			break;
		}
		if (cursor >= 0 && cursor <= 3) {
			mem_saveCfg((uint8_t*) &cfg);
			HAL_NVIC_SystemReset();
		}
		cursor = 0;
	} else if (key == KEY_BACK) {
		menu = MENU_MAIN;
		cursor = 6;
	}
}

void menu_devinfo(void) {
	u8g2_ClearBuffer(&u8g2);

	u8g2_SetFont(&u8g2, u8g2_font_profont11_tf);
	u8g2_SetDrawColor(&u8g2, 2);
	u8g2_SetFontMode(&u8g2, 1);
	sprintf(dispBuf, "DEVICE INFO (%u/%u)", pagenum, numofpages);
	uint16_t strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	uint16_t xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 8, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 10, 128);

	switch (pagenum) {
	case 1:
		sprintf(dispBuf, "SLAVE ADDR: %u", cfg.mbAddr);
		u8g2_DrawStr(&u8g2, 0, 21, dispBuf);
		sprintf(dispBuf, "BAUD RATE : %lu", (uint32_t) cfg.mbBaudrate * 100);
		u8g2_DrawStr(&u8g2, 0, 32, dispBuf);
		if (cfg.mbParity == PARITY_ODD) {
			sprintf(dispBuf, "PARITY    : ODD");
			u8g2_DrawStr(&u8g2, 0, 43, dispBuf);
			sprintf(dispBuf, "STOP BIT  : 1");
			u8g2_DrawStr(&u8g2, 0, 54, dispBuf);
		} else if (cfg.mbParity == PARITY_EVEN) {
			sprintf(dispBuf, "PARITY    : EVEN");
			u8g2_DrawStr(&u8g2, 0, 43, dispBuf);
			sprintf(dispBuf, "STOP BIT  : 1");
			u8g2_DrawStr(&u8g2, 0, 54, dispBuf);
		} else {
			sprintf(dispBuf, "PARITY    : NONE");
			u8g2_DrawStr(&u8g2, 0, 43, dispBuf);
			sprintf(dispBuf, "STOP BITS : 2");
			u8g2_DrawStr(&u8g2, 0, 54, dispBuf);
		}
		break;
	case 2:
		sprintf(dispBuf, "LO THRES: %.3f%cC", cfg.thresholdLo, 176);
		u8g2_DrawStr(&u8g2, 0, 21, dispBuf);
		sprintf(dispBuf, "HI THRES: %.3f%cC", cfg.thresholdHi, 176);
		u8g2_DrawStr(&u8g2, 0, 32, dispBuf);
		sprintf(dispBuf, "ZERO: %.3f ohm", cfg.calZero);
		u8g2_DrawStr(&u8g2, 0, 43, dispBuf);
		sprintf(dispBuf, "SPAN: %.3f ohm", cfg.calSpan);
		u8g2_DrawStr(&u8g2, 0, 54, dispBuf);
		break;
	case 3:
		sprintf(dispBuf, "RTD TYPE: PT100");
		u8g2_DrawStr(&u8g2, 0, 21, dispBuf);
		sprintf(dispBuf, "WIRE CFG: 3-WIRE");
		u8g2_DrawStr(&u8g2, 0, 32, dispBuf);
		if (cfg.rtotEquation == EQ_RPOLY)
			sprintf(dispBuf, "T(R) EQ.: R POLY");
		else if (cfg.rtotEquation == EQ_POLY)
			sprintf(dispBuf, "T(R) EQ.: POLY");
		else if (cfg.rtotEquation == EQ_CUBE)
			sprintf(dispBuf, "T(R) EQ.: CUBIC");
		else
			sprintf(dispBuf, "T(R) EQ.: CVD");
		u8g2_DrawStr(&u8g2, 0, 43, dispBuf);
//		sprintf(dispBuf, "ELEMENTS: FILM");
//		u8g2_DrawStr(&u8g2, 0, 43, dispBuf);
//		sprintf(dispBuf, "CLASS   : F 0.15");
//		u8g2_DrawStr(&u8g2, 0, 54, dispBuf);
		break;
	default:
		break;
	}

	u8g2_SetFont(&u8g2, u8g2_font_profont10_tf);
	sprintf(dispBuf, "EXIT | PREV | NEXT |     ");
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 64, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 56, 128);

	u8g2_SendBuffer(&u8g2);

	if (key == KEY_INC) {
		pagenum++;
		if (pagenum > 3) {
			pagenum = 1;
		}
	} else if (key == KEY_DEC) {
		pagenum--;
		if (pagenum < 1) {
			pagenum = 3;
		}
	} else if (key == KEY_BACK) {
		menu = MENU_MAIN;
		cursor = 7;
	}
}

//PV = temperature, TV = adc code, QV = fault
void menu_cal_zero(float PV, uint16_t TV, uint8_t QV) {
	cal_zero_temp = pt100rtd_resistance(pt100rtd_ratio((float) TV));

	u8g2_ClearBuffer(&u8g2);

	u8g2_SetFont(&u8g2, u8g2_font_profont11_tf);
	u8g2_SetDrawColor(&u8g2, 2);
	u8g2_SetFontMode(&u8g2, 1);

	sprintf(dispBuf, "ZERO CALIBRATION");
	uint16_t strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	uint16_t xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 8, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 10, 128);

	sprintf(dispBuf, "0%cC (100.0 ohm)", 176);
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 24, dispBuf);

	sprintf(dispBuf, "SAVED: %.3f ohm", cfg.calZero);
	u8g2_DrawStr(&u8g2, 0, 37, dispBuf);

	sprintf(dispBuf, "NEW  : %.3f ohm", cal_zero_temp);
	u8g2_DrawStr(&u8g2, 0, 48, dispBuf);

	sprintf(dispBuf, "EXIT|    |    |NEXT");
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 63, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 54, 128);

	u8g2_SendBuffer(&u8g2);

	if (key == KEY_OK) {
		menu = MENU_CAL_SPAN;
		cursor = 0;
	} else if (key == KEY_BACK) {
		menu = MENU_MAIN;
		cursor = 3;
	}
}

//PV = temperature, TV = adc code, QV = fault
void menu_cal_span(float PV, uint16_t TV, uint8_t QV) {
	cal_span_temp = pt100rtd_resistance(pt100rtd_ratio((float) TV));

	u8g2_ClearBuffer(&u8g2);

	u8g2_SetFont(&u8g2, u8g2_font_profont11_tf);
	u8g2_SetDrawColor(&u8g2, 2);
	u8g2_SetFontMode(&u8g2, 1);

	sprintf(dispBuf, "SPAN CALIBRATION");
	uint16_t strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	uint16_t xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 8, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 10, 128);

	sprintf(dispBuf, "100%cC (138.505 ohm)", 176);
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 24, dispBuf);

	sprintf(dispBuf, "SAVED: %.3f ohm", cfg.calSpan);
	u8g2_DrawStr(&u8g2, 0, 37, dispBuf);

	sprintf(dispBuf, "NEW  : %.3f ohm", cal_span_temp);
	u8g2_DrawStr(&u8g2, 0, 48, dispBuf);

	sprintf(dispBuf, "BACK|    |    |SAVE");
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 63, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 54, 128);

	u8g2_SendBuffer(&u8g2);

	if (key == KEY_OK) {
		cfg.calZero = cal_zero_temp;
		cfg.calSpan = cal_span_temp;
		mem_saveCfg((uint8_t*) &cfg);
		HAL_NVIC_SystemReset();
	} else if (key == KEY_BACK) {
		menu = MENU_CAL_ZERO;
		cursor = 0;
	}
}

void menu_thres_manual_lo(void) {
	u8g2_ClearBuffer(&u8g2);

	u8g2_SetFont(&u8g2, u8g2_font_profont11_tf);
	u8g2_SetDrawColor(&u8g2, 2);
	u8g2_SetFontMode(&u8g2, 1);
	sprintf(dispBuf, "LOW THRES. (MANUAL)");
	uint16_t strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	uint16_t xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 8, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 10, 128);

	sprintf(dispBuf, "SAVED: %.3f%cC", cfg.thresholdLo, 176);
	u8g2_DrawStr(&u8g2, 0, 27, dispBuf);

	sprintf(dispBuf, "NEW: < %.3f%cC >", thres_lo_c_temp, 176);
	u8g2_DrawStr(&u8g2, 0, 44, dispBuf);

	sprintf(dispBuf, "EXIT|DEC-|INC+|NEXT");
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 63, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 54, 128);

	u8g2_SendBuffer(&u8g2);

	if (key == KEY_OK) {
		menu = MENU_THRES_MANUAL_HI;
		cursor = 0;
	} else if (key == KEY_DEC) {
		thres_lo_c_temp -= (float) 10;
		if (thres_lo_c_temp < DEFAULT_LRV_C) {
			thres_lo_c_temp = DEFAULT_URV_C;
		}
	} else if (key == KEY_INC) {
		thres_lo_c_temp += (float) 10;
		if (thres_lo_c_temp > DEFAULT_URV_C) {
			thres_lo_c_temp = DEFAULT_LRV_C;
		}
	} else if (key == KEY_BACK) {
		menu = MENU_MAIN;
		cursor = 5;
	}
}

void menu_thres_manual_hi(void) {
	u8g2_ClearBuffer(&u8g2);

	u8g2_SetFont(&u8g2, u8g2_font_profont11_tf);
	u8g2_SetDrawColor(&u8g2, 2);
	u8g2_SetFontMode(&u8g2, 1);
	sprintf(dispBuf, "HIGH THRES. (MANUAL)");
	uint16_t strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	uint16_t xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 8, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 10, 128);

	sprintf(dispBuf, "SAVED: %.3f%cC", cfg.thresholdHi, 176);
	u8g2_DrawStr(&u8g2, 0, 27, dispBuf);

	sprintf(dispBuf, "NEW: < %.3f%cC >", thres_hi_c_temp, 176);
	u8g2_DrawStr(&u8g2, 0, 44, dispBuf);

	sprintf(dispBuf, "BACK|DEC-|INC+|SAVE");
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 63, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 54, 128);

	u8g2_SendBuffer(&u8g2);

	if (key == KEY_OK) {
		cfg.thresholdRawLo = 0;
		cfg.thresholdRawHi = 0xFFFF;
		cfg.thresholdLo = thres_lo_c_temp;
		cfg.thresholdHi = thres_hi_c_temp;
		mem_saveCfg((uint8_t*) &cfg);
		HAL_NVIC_SystemReset();
	} else if (key == KEY_DEC) {
		thres_hi_c_temp -= (float) 10;
		if (thres_hi_c_temp < DEFAULT_LRV_C) {
			thres_hi_c_temp = DEFAULT_URV_C;
		}
	} else if (key == KEY_INC) {
		thres_hi_c_temp += (float) 10;
		if (thres_hi_c_temp > DEFAULT_URV_C) {
			thres_hi_c_temp = DEFAULT_LRV_C;
		}
	} else if (key == KEY_BACK) {
		menu = MENU_THRES_MANUAL_LO;
		cursor = 0;
	}
}

//PV = temperature, SV = resistance, TV = adc code, QV = fault
void menu_thres_auto_lo(float PV, uint16_t TV, uint8_t QV) {
	thres_lo_raw_temp = TV;
	thres_lo_c_temp = PV;

	u8g2_ClearBuffer(&u8g2);

	u8g2_SetFont(&u8g2, u8g2_font_profont11_tf);
	u8g2_SetDrawColor(&u8g2, 2);
	u8g2_SetFontMode(&u8g2, 1);
	sprintf(dispBuf, "LOW THRESHOLD (AUTO)");
	uint16_t strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	uint16_t xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 8, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 10, 128);

	sprintf(dispBuf, "SAVED: %.3f%cC", cfg.thresholdLo, 176);
	u8g2_DrawStr(&u8g2, 0, 27, dispBuf);

	sprintf(dispBuf, "NEW  : %.3f%cC", PV, 176);
	u8g2_DrawStr(&u8g2, 0, 44, dispBuf);

	sprintf(dispBuf, "EXIT|    |    |NEXT");
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 63, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 54, 128);

	u8g2_SendBuffer(&u8g2);

	if (key == KEY_OK) {
		menu = MENU_THRES_AUTO_HI;
		cursor = 0;
	} else if (key == KEY_BACK) {
		menu = MENU_MAIN;
		cursor = 4;
	}
}

//PV = temperature, SV = resistance, TV = adc code, QV = fault
void menu_thres_auto_hi(float PV, uint16_t TV, uint8_t QV) {
	thres_hi_raw_temp = TV;
	thres_hi_c_temp = PV;

	u8g2_ClearBuffer(&u8g2);

	u8g2_SetFont(&u8g2, u8g2_font_profont11_tf);
	u8g2_SetDrawColor(&u8g2, 2);
	u8g2_SetFontMode(&u8g2, 1);
	sprintf(dispBuf, "HIGH THRESHOLD (AUTO)");
	uint16_t strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	uint16_t xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 8, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 10, 128);

	sprintf(dispBuf, "SAVED: %.3f%cC", cfg.thresholdHi, 176);
	u8g2_DrawStr(&u8g2, 0, 27, dispBuf);

	sprintf(dispBuf, "NEW  : %.3f%cC", PV, 176);
	u8g2_DrawStr(&u8g2, 0, 44, dispBuf);

	sprintf(dispBuf, "BACK|    |    |SAVE");
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 63, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 54, 128);

	u8g2_SendBuffer(&u8g2);

	if (key == KEY_OK) {
		cfg.thresholdRawLo = thres_lo_raw_temp;
		cfg.thresholdRawHi = thres_hi_raw_temp;
		cfg.thresholdLo = thres_lo_c_temp;
		cfg.thresholdHi = thres_hi_c_temp;
		mem_saveCfg((uint8_t*) &cfg);
		HAL_NVIC_SystemReset();
	} else if (key == KEY_BACK) {
		menu = MENU_THRES_AUTO_LO;
		cursor = 0;
	}
}

void menu_mb_slaveaddr(void) {
	u8g2_ClearBuffer(&u8g2);

	u8g2_SetFont(&u8g2, u8g2_font_spleen16x32_mr);
	u8g2_SetDrawColor(&u8g2, 2);
	u8g2_SetFontMode(&u8g2, 1);

	sprintf(dispBuf, "< %03u >", mb_slave_addr_temp);
	uint16_t strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	uint16_t xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 42, dispBuf);

	u8g2_SetFont(&u8g2, u8g2_font_profont11_tr);
	u8g2_SetDrawColor(&u8g2, 2);
	u8g2_SetFontMode(&u8g2, 1);

	sprintf(dispBuf, "SET SLAVE ADDRESS");
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 8, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 10, 128);

	sprintf(dispBuf, "EXIT|DEC-|INC+|SAVE");
	strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, 63, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, 54, 128);

	u8g2_SendBuffer(&u8g2);

	if (key == KEY_DEC) {
		mb_slave_addr_temp--;
		if (mb_slave_addr_temp < 1) {
			mb_slave_addr_temp = MAX_SLAVEID;
		}
	} else if (key == KEY_INC) {
		mb_slave_addr_temp++;
		if (mb_slave_addr_temp > MAX_SLAVEID) {
			mb_slave_addr_temp = 1;
		}
	} else if (key == KEY_BACK) {
		menu = MENU_MAIN;
		cursor = 0;
	} else if (key == KEY_OK) {
		cfg.mbAddr = mb_slave_addr_temp;
		mem_saveCfg((uint8_t*) &cfg);
		HAL_NVIC_SystemReset();
	}
}

#define NUM_BAUDRATE 9
const char *baudratestr[] = { "1200", "2400", "4800", "9600", "19200", "38400",
		"57600", "115200", "EXIT/BACK", NULL };
void menu_mb_baudrate(void) {
	u8g2_ClearBuffer(&u8g2);

	uint8_t ycoord = 0, startmenu = 0;
	u8g2_SetFont(&u8g2, u8g2_font_profont11_tr);
	u8g2_SetDrawColor(&u8g2, 2);
	u8g2_SetFontMode(&u8g2, 1);
	ycoord += 7;
	sprintf(dispBuf, "SET BAUD RATE");
	uint16_t strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	uint16_t xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, ycoord, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, ycoord + 1, 128);
	ycoord += 2;
	if (key == KEY_DEC) {
		if (cursor < NUM_BAUDRATE - 1) {
			cursor++;
		} else {
			cursor = 0;
		}
	} else if (key == KEY_INC) {
		if (cursor > 0) {
			cursor--;
		} else {
			cursor = NUM_BAUDRATE - 1;
		}
	} else if (key == KEY_BACK) {
		menu = MENU_MAIN;
		cursor = NUM_BAUDRATE - 1;
	}
	if (cursor > MAX_MENU_PER_SCREEN - 2) {
		startmenu = cursor - (MAX_MENU_PER_SCREEN - 2);
	}
	//printf("awal:%d, cn:%d\r\n",startmenu,cursor);
	for (uint8_t i = startmenu; i < startmenu + MAX_MENU_PER_SCREEN; i++) {
		ycoord += SPACE_ROW;
		if (baudratestr[i] == NULL)
			break;
		u8g2_DrawStr(&u8g2, OFFSET_LEFT, ycoord, baudratestr[i]);
		if (cursor == i) {
			u8g2_DrawBox(&u8g2, 0, ycoord - SPACE_ROW + 2, 128, SPACE_ROW);
		}
		//printf("i:%d, ycoord:%d,%s\r\n",i,ycoord,baudratestr[i]);
	}

	u8g2_SendBuffer(&u8g2);

	if (key == KEY_OK) {
		switch (cursor) {
		case 0:
			cfg.mbBaudrate = BR_1200;
			break;
		case 1:
			cfg.mbBaudrate = BR_2400;
			break;
		case 2:
			cfg.mbBaudrate = BR_4800;
			break;
		case 3:
			cfg.mbBaudrate = BR_9600;
			break;
		case 4:
			cfg.mbBaudrate = BR_19200;
			break;
		case 5:
			cfg.mbBaudrate = BR_38400;
			break;
		case 6:
			cfg.mbBaudrate = BR_57600;
			break;
		case 7:
			cfg.mbBaudrate = BR_115200;
			break;
		case 8:
			menu = MENU_MAIN;
			break;
		default:
//			menu = MENU_MB_BAUDRATE;
			break;
		}
		if (cursor >= 0 && cursor <= 7) {
			mem_saveCfg((uint8_t*) &cfg);
			HAL_NVIC_SystemReset();
		}
		cursor = 1;
	} else if (key == KEY_BACK) {
		menu = MENU_MAIN;
		cursor = 1;
	}
}

#define NUM_PARITY 4
const char *paritystr[] = { "NONE", "EVEN", "ODD", "EXIT/BACK", NULL };
void menu_mb_parity(void) {
	u8g2_ClearBuffer(&u8g2);

	uint8_t ycoord = 0, startmenu = 0;
	u8g2_SetFont(&u8g2, u8g2_font_profont11_tr);
	u8g2_SetDrawColor(&u8g2, 2);
	u8g2_SetFontMode(&u8g2, 1);
	ycoord += 7;
	sprintf(dispBuf, "SET PARITY");
	uint16_t strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	uint16_t xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, ycoord, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, ycoord + 1, 128);
	ycoord += 2;
	if (key == KEY_DEC) {
		if (cursor < NUM_PARITY - 1) {
			cursor++;
		} else {
			cursor = 0;
		}
	} else if (key == KEY_INC) {
		if (cursor > 0) {
			cursor--;
		} else {
			cursor = NUM_PARITY - 1;
		}
	} else if (key == KEY_BACK) {
		menu = MENU_MAIN;
		cursor = NUM_PARITY - 1;
	}
	if (cursor > MAX_MENU_PER_SCREEN - 2) {
		startmenu = cursor - (MAX_MENU_PER_SCREEN - 2);
	}
	//printf("awal:%d, cn:%d\r\n",startmenu,cursor);
	for (uint8_t i = startmenu; i < startmenu + MAX_MENU_PER_SCREEN; i++) {
		ycoord += SPACE_ROW;
		if (paritystr[i] == NULL)
			break;
		u8g2_DrawStr(&u8g2, OFFSET_LEFT, ycoord, paritystr[i]);
		if (cursor == i) {
			u8g2_DrawBox(&u8g2, 0, ycoord - SPACE_ROW + 2, 128, SPACE_ROW);
		}
		//printf("i:%d, ycoord:%d,%s\r\n",i,ycoord,paritystr[i]);
	}

	u8g2_SendBuffer(&u8g2);

	if (key == KEY_OK) {
		switch (cursor) {
		case 0:
			cfg.mbParity = PARITY_NONE;
			break;
		case 1:
			cfg.mbParity = PARITY_EVEN;
			break;
		case 2:
			cfg.mbParity = PARITY_ODD;
			break;
		case 3:
			menu = MENU_MAIN;
			break;
		default:
//			menu = MENU_MB_PARITY;
			break;
		}
		if (cursor >= 0 && cursor <= 2) {
			mem_saveCfg((uint8_t*) &cfg);
			HAL_NVIC_SystemReset();
		}
		cursor = 2;
	} else if (key == KEY_BACK) {
		menu = MENU_MAIN;
		cursor = 2;
	}
}

#define NUM_MENUMAIN 11
const char *menumainstr[] = { "MB SLAVE ADDRESS", "MB BAUD RATE", "MB PARITY",
		"CALIBRATION", "THRESHOLD (AUTO)", "THRESHOLD (MANUAL)",
		"T(R) EQUATION", "DEVICE INFO", "DEFAULT CONFIG.",
		"SYSTEM RESET/REBOOT", "EXIT/BACK", NULL };
void menu_main(void) {
	u8g2_ClearBuffer(&u8g2);

	uint8_t ycoord = 0, startmenu = 0;
	u8g2_SetFont(&u8g2, u8g2_font_profont11_tr);
	u8g2_SetDrawColor(&u8g2, 2);
	u8g2_SetFontMode(&u8g2, 1);
	ycoord += 7;
	sprintf(dispBuf, "SELECT MENU");
	uint16_t strwidth = u8g2_GetStrWidth(&u8g2, dispBuf);
	uint16_t xcoord = 64 - (strwidth / 2);
	u8g2_DrawStr(&u8g2, xcoord, ycoord, dispBuf);
	u8g2_DrawHLine(&u8g2, 0, ycoord + 1, 128);
	ycoord += 2;
	if (key == KEY_DEC) {
		if (cursor < NUM_MENUMAIN - 1) {
			cursor++;
		} else {
			cursor = 0;
		}
	} else if (key == KEY_INC) {
		if (cursor > 0) {
			cursor--;
		} else {
			cursor = NUM_MENUMAIN - 1;
		}
	} else if (key == KEY_BACK) {
		menu = MENU_HOME;
		cursor = NUM_MENUMAIN - 1;
	}
	if (cursor > MAX_MENU_PER_SCREEN - 2) {
		startmenu = cursor - (MAX_MENU_PER_SCREEN - 2);
	}
	//printf("awal:%d, cn:%d\r\n",startmenu,cursor);
	for (uint8_t i = startmenu; i < startmenu + MAX_MENU_PER_SCREEN; i++) {
		ycoord += SPACE_ROW;
		if (menumainstr[i] == NULL)
			break;
		u8g2_DrawStr(&u8g2, OFFSET_LEFT, ycoord, menumainstr[i]);
		if (cursor == i) {
			u8g2_DrawBox(&u8g2, 0, ycoord - SPACE_ROW + 2, 128, SPACE_ROW);
		}
		//printf("i:%d, ycoord:%d,%s\r\n",i,ycoord,menumainstr[i]);
	}

	u8g2_SendBuffer(&u8g2);

	if (key == KEY_OK) {
		switch (cursor) {
		case 0:
			menu = MENU_MB_SLAVEADDR;
			mb_slave_addr_temp = cfg.mbAddr;
			cursor = 0;
			break;
		case 1:
			menu = MENU_MB_BAUDRATE;
			cursor = 4;
			break;
		case 2:
			menu = MENU_MB_PARITY;
			cursor = 1;
			break;
		case 3:
			menu = MENU_CAL_ZERO;
			cursor = 0;
			cal_zero_temp = cfg.calZero;
			cal_span_temp = cfg.calSpan;
			break;
		case 4:
			menu = MENU_THRES_AUTO_LO;
			thres_hi_c_temp = cfg.thresholdHi;
			thres_lo_c_temp = cfg.thresholdLo;
			thres_hi_raw_temp = cfg.thresholdRawHi;
			thres_lo_raw_temp = cfg.thresholdRawLo;
			cursor = 0;
			break;
		case 5:
			menu = MENU_THRES_MANUAL_LO;
			thres_hi_c_temp = cfg.thresholdHi;
			thres_lo_c_temp = cfg.thresholdLo;
			thres_hi_raw_temp = cfg.thresholdRawHi;
			thres_lo_raw_temp = cfg.thresholdRawLo;
			cursor = 0;
			break;
		case 6:
			menu = MENU_RTOT_EQUATION;
			cursor = 0;
			break;
		case 7:
			menu = MENU_DEVINFO;
			cursor = 0;
			pagenum = 1;
			numofpages = 3;
			break;
		case 8:
			mem_saveDefaultCfg();
			HAL_NVIC_SystemReset();
			break;
		case 9:
			HAL_NVIC_SystemReset();
			break;
		case 10:
			menu = MENU_HOME;
			cursor = 0;
			break;
		default:
//			menu = MENU_MAIN;
			break;
		}
	}

}

//PV = temperature, SV = resistance, TV = adc code, QV = fault
void gui_loop(float PV, float SV, uint16_t TV, uint8_t QV) {
#if 0
	if (menu == MENU_MB_SLAVEADDR || menu == MENU_THRES_MANUAL_HI
			|| menu == MENU_THRES_MANUAL_LO) {
		key = KeyPad_Scan();
		osDelay(20);
	} else {
		key = KeyPad_WaitForKey(100);
	}
#else
	key = KeyPad_WaitForKey(100);
#endif
#if 0
//	if (key != KEY_NONE) {
		printf("[GUI] KEY released: 0x%04X\r\n", key);
		printf("[GUI] KEY last   : 0x%04X\r\n", KeyPad_LastKey());
//	}
#endif

	switch (menu) {
	case MENU_HOME:
		menu_home(PV, QV);
		break;
	case MENU_MB_SLAVEADDR:
		menu_mb_slaveaddr();
		break;
	case MENU_MB_BAUDRATE:
		menu_mb_baudrate();
		break;
	case MENU_MB_PARITY:
		menu_mb_parity();
		break;
	case MENU_CAL_ZERO:
		menu_cal_zero(PV, TV, QV);
		break;
	case MENU_CAL_SPAN:
		menu_cal_span(PV, TV, QV);
		break;
	case MENU_THRES_AUTO_LO:
		menu_thres_auto_lo(PV, TV, QV);
		break;
	case MENU_THRES_AUTO_HI:
		menu_thres_auto_hi(PV, TV, QV);
		break;
	case MENU_THRES_MANUAL_LO:
		menu_thres_manual_lo();
		break;
	case MENU_THRES_MANUAL_HI:
		menu_thres_manual_hi();
		break;
	case MENU_RTOT_EQUATION:
		menu_rtot_equation();
		break;
	case MENU_DEVINFO:
		menu_devinfo();
		break;
	case MENU_MAIN:
		menu_main();
	default:
//		menu_home(PV, QV);
		break;
	}
}
