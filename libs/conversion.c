/*
 * https://github.com/arduino-libraries/ArduinoModbus/blob/master/src/libmodbus/modbus-data.c
 * https://github.com/DFRobot/STM32/blob/master/libraries/DFRobotBlunoM3/util.h
 * https://github.com/belyalov/stm32-hal-libraries/blob/master/htons.h
 * https://github.com/joeferner/stm32-network/blob/master/interface.h
 */

/*
 * conversion.c
 *
 *  Created on: Nov 21, 2023
 *      Author: isa
 *
 * abcd = little-endian
 * dcba = big-endian
 * badc = little-endian byte swap
 * cdab = big-endian byte swap
 */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#if 0
#define htons(x) ( ((x)<<8) | (((x)>>8)&0xFF) )
#define ntohs(x) htons(x)

#define htonl(x) ( ((x)<<24 & 0xFF000000UL) | \
                   ((x)<< 8 & 0x00FF0000UL) | \
                   ((x)>> 8 & 0x0000FF00UL) | \
                   ((x)>>24 & 0x000000FFUL) )
#define ntohl(x) htonl(x)

#define bswap_16 __builtin_bswap16
#define bswap_32 __builtin_bswap32
#endif

#define bswap_16 __builtin_bswap16
#define bswap_32 __builtin_bswap32

#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define htonl(x) bswap_32(x)
#define htons(x) bswap_16(x)
#define ntohl(x) bswap_32(x)
#define ntohs(x) bswap_16(x)
#else
#define htonl(x) (x)
#define htons(x) (x)
#define ntohl(x) (x)
#define ntohs(x) (x)
#endif

/* Sets many bits from a single byte value (all 8 bits of the byte value are
 set) */
void mb_set_bits_from_byte(uint8_t *dest, int idx, const uint8_t value) {
	int i;

	for (i = 0; i < 8; i++) {
		dest[idx + i] = (value & (1 << i)) ? 1 : 0;
	}
}

/* Sets many bits from a table of bytes (only the bits between idx and
 idx + nb_bits are set) */
void mb_set_bits_from_bytes(uint8_t *dest, int idx, unsigned int nb_bits,
		const uint8_t *tab_byte) {
	unsigned int i;
	int shift = 0;

	for (i = idx; i < idx + nb_bits; i++) {
		dest[i] = tab_byte[(i - idx) / 8] & (1 << shift) ? 1 : 0;
		/* gcc doesn't like: shift = (++shift) % 8; */
		shift++;
		shift %= 8;
	}
}

/* Gets the byte value from many bits.
 To obtain a full byte, set nb_bits to 8. */
uint8_t mb_get_byte_from_bits(const uint8_t *src, int idx,
		unsigned int nb_bits) {
	unsigned int i;
	uint8_t value = 0;

	if (nb_bits > 8) {
		/* Assert is ignored if NDEBUG is set */
		assert(nb_bits < 8);
		nb_bits = 8;
	}

	for (i = 0; i < nb_bits; i++) {
		value |= (src[idx + i] << i);
	}

	return value;
}

/* Get a float from 4 bytes (Modbus) without any conversion (ABCD) */
float mb_get_float_abcd(const uint16_t *src, uint8_t idxHi, uint8_t idxLo) {
	float f;
	uint32_t i;

	i = ntohl(((uint32_t )src[idxHi] << 16) + src[idxLo]);
	memcpy(&f, &i, sizeof(float));

	return f;
}

/* Get a float from 4 bytes (Modbus) in inversed format (DCBA) */
float mb_get_float_dcba(const uint16_t *src, uint8_t idxHi, uint8_t idxLo) {
	float f;
	uint32_t i;

	i = ntohl(bswap_32((((uint32_t)src[idxHi]) << 16) + src[idxLo]));
	memcpy(&f, &i, sizeof(float));

	return f;
}

/* Get a float from 4 bytes (Modbus) with swapped bytes (BADC) */
float mb_get_float_badc(const uint16_t *src, uint8_t idxHi, uint8_t idxLo) {
	float f;
	uint32_t i;

#if defined(ARDUINO) && defined(__AVR__)
    i = ntohl((uint32_t)((uint32_t)bswap_16(src[idxHi]) << 16) + bswap_16(src[idxLo]));
#else
	i = ntohl((uint32_t )(bswap_16(src[idxHi]) << 16) + bswap_16(src[idxLo]));
#endif
	memcpy(&f, &i, sizeof(float));

	return f;
}

/* Get a float from 4 bytes (Modbus) with swapped words (CDAB) */
float mb_get_float_cdab(const uint16_t *src, uint8_t idxHi, uint8_t idxLo) {
	float f;
	uint32_t i;

	i = ntohl((((uint32_t )src[idxLo]) << 16) + src[idxHi]);
	memcpy(&f, &i, sizeof(float));

	return f;
}

/* DEPRECATED - Get a float from 4 bytes in sort of Modbus format */
float mb_get_float(const uint16_t *src, uint8_t idxHi, uint8_t idxLo) {
	float f;
	uint32_t i;

	i = (((uint32_t) src[idxLo]) << 16) + src[idxHi];
	memcpy(&f, &i, sizeof(float));

	return f;
}

/* Set a float to 4 bytes for Modbus w/o any conversion (ABCD) */
void mb_set_float_abcd(float f, uint16_t *dest, uint8_t hiIdx, uint8_t loIdx) {
	uint32_t i;

	memcpy(&i, &f, sizeof(uint32_t));
	i = htonl(i);
	dest[hiIdx] = (uint16_t) (i >> 16);
	dest[loIdx] = (uint16_t) i;
}

/* Set a float to 4 bytes for Modbus with byte and word swap conversion (DCBA) */
void mb_set_float_dcba(float f, uint16_t *dest, uint8_t hiIdx, uint8_t loIdx) {
	uint32_t i;

	memcpy(&i, &f, sizeof(uint32_t));
	i = bswap_32(htonl(i));
	dest[hiIdx] = (uint16_t) (i >> 16);
	dest[loIdx] = (uint16_t) i;
}

/* Set a float to 4 bytes for Modbus with byte swap conversion (BADC) */
void mb_set_float_badc(float f, uint16_t *dest, uint8_t hiIdx, uint8_t loIdx) {
	uint32_t i;

	memcpy(&i, &f, sizeof(uint32_t));
	i = htonl(i);
	dest[hiIdx] = (uint16_t) bswap_16(i >> 16);
	dest[loIdx] = (uint16_t) bswap_16(i & 0xFFFF);
}

/* Set a float to 4 bytes for Modbus with word swap conversion (CDAB) */
void mb_set_float_cdab(float f, uint16_t *dest, uint8_t hiIdx, uint8_t loIdx) {
	uint32_t i;

	memcpy(&i, &f, sizeof(uint32_t));
	i = htonl(i);
	dest[hiIdx] = (uint16_t) i;
	dest[loIdx] = (uint16_t) (i >> 16);
}

/* DEPRECATED - Set a float to 4 bytes in a sort of Modbus format! */
void mb_set_float(float f, uint16_t *dest, uint8_t hiIdx, uint8_t loIdx) {
	uint32_t i;

	memcpy(&i, &f, sizeof(uint32_t));
	dest[hiIdx] = (uint16_t) i;
	dest[loIdx] = (uint16_t) (i >> 16);
}
