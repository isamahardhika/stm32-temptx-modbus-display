/*
 * refer to:
 * https://github.com/drhaney/pt100rtd
 * http://www.mosaic-industries.com/embedded-systems/microcontroller-projects/temperature-measurement/platinum-rtd-sensors/resistance-calibration-table
 */

/*
 * pt100rtd.h
 *
 *  Created on: Dec 6, 2023
 *      Author: isa
 */

#ifndef PT100RTD_H_
#define PT100RTD_H_

float pt100rtd_ratio(float ADC_code);
float pt100rtd_resistance(float Ratio);
float pt100rtd_celsius_cubic(float R_ohms);
float pt100rtd_celsius_cvd(float R_ohms);
float pt100rtd_celsius_polynomial(float R_ohms);
float pt100rtd_celsius_rationalpoly(float R_ohms);

#endif /* PT100RTD_H_ */
