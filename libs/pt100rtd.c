/*
 * refer to:
 * https://github.com/drhaney/pt100rtd
 * http://www.mosaic-industries.com/embedded-systems/microcontroller-projects/temperature-measurement/platinum-rtd-sensors/resistance-calibration-table
 */

/*
 * pt100rtd.c
 *
 *  Created on: Dec 6, 2023
 *      Author: isa
 */

#include <stdint.h>
#include <math.h>
//#include "config.h"
//#include "MAX31865.h"
#include "MAX31865Conf.h"
#include "pt100rtd.h"

float pt100rtd_ratio(float ADC_code) {
	ADC_code /= (float) 32768;
	return ADC_code;
}

float pt100rtd_resistance(float Ratio) {
	return Ratio *= _MAX31865_RREF;
}

// cubic approximation
float pt100rtd_celsius_cubic(float R_ohms) {
	float T = -247.29
			+ R_ohms * (2.3992 + R_ohms * (0.00063962 + 1.0241E-6 * R_ohms));
	return T;
}

// inverse Callendar-Van Dusen formula.
// accurate from -60C up to 850 C.
float pt100rtd_celsius_cvd(float R_ohms) {
	float Z1, Z2, Z3, Z4, temp;

	//Serial.print("Resistance: "); Serial.println(Rt, 8);

	Z1 = -iCVD_A;
	Z2 = iCVD_A * iCVD_A - (4 * iCVD_B);
	Z3 = (4 * iCVD_B) / _MAX31865_RNOMINAL;
	Z4 = 2 * iCVD_B;

	temp = Z2 + (Z3 * R_ohms);
	temp = (sqrtf(temp) + Z1) / Z4;

	return temp;
}

// R2T polynomial from Analog Devices AN709 app note.
// implementation ganked from Adafruit MAX31865 library.
// Use for accurate temperatures -60C and below.
// Warning! Exceeds Class B tolerance spec above +164C
float pt100rtd_celsius_polynomial(float R_ohms) {
	float rpoly, temp;
	rpoly = R_ohms;

	temp = -242.02;
	temp += 2.2228 * rpoly;
	rpoly *= R_ohms;			// square ;
	temp += 2.5859e-3 * rpoly;
	rpoly *= R_ohms;			// ^3 ;
	temp -= 4.8260e-6 * rpoly;
	rpoly *= R_ohms;			// ^4 ;
	temp -= 2.8183e-8 * rpoly;
	rpoly *= R_ohms;			// ^5 ;
	temp += 1.5243e-10 * rpoly;

	return temp;
}

// Rational polynomial fraction approximation taken from
// Mosaic-Industries.com page on "RTD calibration."
// Accurate, probably beyond the ITS-90 spec
// Average error ± 0.015°C in range -200°C → +850°C
float pt100rtd_celsius_rationalpoly(float R_ohms) {
	float num, denom, T;

	float c0 = -245.19;
	float c1 = 2.5293;
	float c2 = -0.066046;
	float c3 = 4.0422E-3;
	float c4 = -2.0697E-6;
	float c5 = -0.025422;
	float c6 = 1.6883E-3;
	float c7 = -1.3601E-6;

	num = R_ohms * (c1 + R_ohms * (c2 + R_ohms * (c3 + R_ohms * c4)));
	denom = 1.0 + R_ohms * (c5 + R_ohms * (c6 + R_ohms * c7));
	T = c0 + (num / denom);

	return T;
}
