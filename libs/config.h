/*
 * config.h
 *
 *  Created on: Nov 18, 2023
 *      Author: MMI
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#define RESISTANCE_FILTERED

#define MAX_SLAVEID 247

//#define RTD_PT100 100
//#define RTD_PT1000 1000
//#define RTD_2WIRE 2
//#define RTD_3WIRE 3
//#define RTD_4WIRE 4

#define EMA_FACTOR 0.5f

//#define MAP_RES_ZERO 100.000 // 0 degC
//#define MAP_RES_SPAN 157.325 // 150 degC

//#define MAP_RES_LOW 80.306 // -50 degC
//#define MAP_RES_ZERO 100.0 // 0 degC
//#define MAP_RES_HIGH 157.325 // 150 degC

//kalibrasi zero & span manual dengan resistor axial
#define RES_100OHM 99.75 // nilai resistor 100 ohm yg terbaca multimeter
#define RES_150OHM 146.75 // nilai resistor 150 ohm yg terbaca multimeter

//resistance (ohm) at a certain temperature (degC)
#define PT100_385_NEG196C 20.247
#define PT100_385_NEG100C 60.256
#define PT100_385_NEG60C 76.328
#define PT100_385_NEG50C 80.306
#define PT100_385_NEG30C 88.222
#define PT100_385_0C 100.000
#define PT100_385_POS50C 119.397
#define PT100_385_POS100C 138.505
#define PT100_385_POS150C 157.325
#define PT100_385_POS200C 175.856
#define PT100_385_POS250C 194.098
#define PT100_385_POS300C 212.051
#define PT100_385_POS350C 229.716
#define PT100_385_POS400C 247.092
#define PT100_385_POS450C 264.179
#define PT100_385_POS500C 280.978
#define PT100_385_POS550C 297.487
#define PT100_385_POS600C 313.708

#define DEFAULT_DEVICE_UID 0x5B06
#define DEFAULT_MB_SLAVEID 1 //modbus address: 1 - 247
//#define DEFAULT_MB_BAUDRATE 192 //19200 baudrate
//#define DEFAULT_MB_WORDLENGTH UART_WORDLENGTH_9B
//#define DEFAULT_MB_STOPBITS UART_STOPBITS_1
//#define DEFAULT_MB_PARITY 2 //EVEN parity

#define DEFAULT_LRV_C -200.0 //in Celsius
#define DEFAULT_URV_C 850.0 //in Celsius
//#define DEFAULT_LRV_F -58 //in Fahrenheit
//#define DEFAULT_URV_F 752 //in Fahrenheit

//#define DEFAULT_THRESHOLD_LO_OHM 100.0 //in Celsius
//#define DEFAULT_THRESHOLD_HI_OHM 229.716 //in Celsius
#define DEFAULT_THRESHOLD_LO_C 0.0f //in Celsius
#define DEFAULT_THRESHOLD_HI_C 150.0f //in Celsius
//#define DEFAULT_THRESHOLD_LO_F 32 //in Fahrenheit
//#define DEFAULT_THRESHOLD_HI_F 662 //in Fahrenheit

#define DEFAULT_THRESHOLD_LO_RAW 0x00 //adc code in hex
#define DEFAULT_THRESHOLD_HI_RAW 0xFFFF //adc code in hex

#define DEFAULT_ZERO PT100_385_0C //in ohm (resistance)
#define DEFAULT_SPAN PT100_385_POS100C //in ohm (resistance)

enum ModbusAddress {
//	DEVICE_UID = 0,
	MB_SLAVEID = 0,
	MB_BAUDRATE,
//	MB_WORDLENGTH,
//	MB_STOPBITS,
	MB_PARITY,
	RTOT_EQUATION,
//	TEMPERATURE_UNIT,
//	RANGE_LOWER_MSB,
//	RANGE_LOWER_LSB,
//	RANGE_UPPER_MSB,
//	RANGE_UPPER_LSB,
//	TOLERANCE_CLASS,
	CAL_ZERO_MSB,
	CAL_ZERO_LSB,
	CAL_SPAN_MSB,
	CAL_SPAN_LSB,
	THRESHOLD_LO_MSB,
	THRESHOLD_LO_LSB,
	THRESHOLD_HI_MSB,
	THRESHOLD_HI_LSB,
	SENSOR_TYPE,
	WIRE_CFG,
	STATUS_FAULT,
	STATUS_ALARM,
	TEMPERATURE_VAL_MSB,
	TEMPERATURE_VAL_LSB,
	RESISTANCE_VAL_MSB,
	RESISTANCE_VAL_LSB,
//	RATIO_VAL_MSB,
//	RATIO_VAL_LSB,
	MAX_MB_ADDR
};
//#define MAX_MB_ADDR MAX_MB_ADDRESS

enum BaudRate {
	BR_1200 = 12,
	BR_2400 = 24,
	BR_4800 = 48,
	BR_9600 = 96,
	BR_19200 = 192,
	BR_38400 = 384,
	BR_57600 = 576,
	BR_115200 = 1152
};

enum Parity {
	PARITY_NONE = 1, PARITY_EVEN, PARITY_ODD
};

enum SensorType {
	SENSOR_PT100 = 100, SENSOR_PT1000 = 1000
};

enum WireType {
	WIRE_2 = 2, WIRE_3, WIRE_4
};

enum AlarmStatus {
	ALARM_NONE = 0,
	ALARM_RTD,
	ALARM_THRESHOLD_LOW,
	ALARM_THRESHOLD_HIGH
};

#if 1
enum TemperatureUnit {
	UNIT_CELSIUS = 1, UNIT_FAHRENHEIT
};

enum ToleranceClass {
	CLASS_AA = 1, CLASS_A, CLASS_B, CLASS_C
};
#endif

enum RtoT_Equation {
	EQ_CVD = 0,
	EQ_CUBE,
	EQ_POLY,
	EQ_RPOLY,
	EQ_LUT
};

typedef struct {
	uint16_t deviceUid; //0x5B06
	uint16_t mbAddr; //1 - 247
	uint16_t mbBaudrate; //mbAddr * 100
	uint16_t mbParity; //NONE = 1, EVEN = 2, ODD = 3
	float lrv; //low range value
	float urv; //upper range value
	float thresholdLo; //low threhsold value in Celsius
	float thresholdHi; //high threshold value in Celsius
	uint16_t thresholdRawLo; //low threhsold value in ADC code
	uint16_t thresholdRawHi; //high threshold value in ADC code
	float calZero; //zero calibration value
	float calSpan; //span calibration value
	uint16_t sensorType; //Pt100 = 100, Pt1000 = 1000
	uint16_t wireCfg; //2-wire = 2, 3-wire = 3, 4-wire = 4
	uint16_t toleranceClass; //AA = 1, A = 2, B = 3, C = 4
	uint16_t tempUnit; //Celsius = 1, Fahrenheit = 2
	uint16_t rtotEquation; //CVD = 0, cubic = 1, polynomial = 2, rational polynomial = 3
} temptx_cfg;

#endif /* CONFIG_H_ */
