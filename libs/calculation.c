/*
 * calculation.c
 *
 *  Created on: Dec 6, 2023
 *      Author: isa
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

float ema_filter(float newIn, float lastOut, float factor) {
	return factor * newIn + (1.0f - factor) * lastOut;
//	return ((float) lastOut * (1.0f - factor)) + ((float) newIn * factor);
}

long map(long x, long in_min, long in_max, long out_min, long out_max) {
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float mapf(float x, float in_min, float in_max, float out_min, float out_max) {
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
