# Industrial Platinum Resistance Thermometers with Modbus RTU (over RS-485 Serial Line) based on STM32F411CE 

This project aims to design and implement a temperature monitoring device or industrial platinum resistance thermometers using multiple hardware components:

- WeAct Studio BlackPill STM32F411CEU6 Core Board
- W25Q64JV 64Mb flash memory (built-in on the board)
- Adafruit MAX31865 PT100 RTD Temperature Sensor Amplifier
- Industrial TTL-RS485 Converter
- 1.3-inch OLED Display (SH1106, I2C, 128x64)
- 1x4 Keypad (BACK, DOWN/DECR., UP/INCR., and OK keys)

The device utilizes a PT100 3-wire RTD sensor for accurate temperature measurement and communicates as a Modbus RTU slave/server over RS485 serial line. Developed using STM32CubeIDE, C programming language, and FreeRTOS middleware, the firmware ensures precise temperature readings within a tolerance of 0.5 degrees Celsius, adhering to IEC 60751 standards. The system also passes the Modbus Conformance Test (Function Code: 3, 4, 6, and 16), affirming the reliability of the Modbus RTU implementation. With an OLED display and keypad interface, the device offers user-friendly interaction. This project serves as a foundation for industrial temperature monitoring applications, demonstrating the integration of RTD, Modbus, and STM32 technologies for efficient automation system end-device.

## Features

- Digital PT100 RTD measurements
- Modbus RTU data communications protocol over RS485 serial line as a slave/server
- Simple GUI


## Acknowledgements

- [Adafruit MAX31865 RTD PT100 or PT1000 Amplifier by lady ada](https://learn.adafruit.com/adafruit-max31865-rtd-pt100-amplifier?view=all)
- [GitHub - alejoseb/Modbus-STM32-HAL-FreeRTOS](https://github.com/alejoseb/Modbus-STM32-HAL-FreeRTOS)
- [GitHub - nimaltd/spif](https://github.com/nimaltd/spif)
- [GitHub - olikraus/u8g2](https://github.com/olikraus/u8g2)
- [GitHub - nimaltd/KeyPad](https://github.com/nimaltd/KeyPad)

## 🚀 About Me

[LinkedIn](https://www.linkedin.com/in/mahardhika-isa/)
